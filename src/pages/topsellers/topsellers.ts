import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {Values} from "../../providers/service/values";
import {Functions} from "../../providers/service/functions";
import {CartPage} from "../cart/cart";
import {ProductPage} from "../product/product";
import {OfflinePage} from "../offline/offline";
import {Network} from "@ionic-native/network";
import {AppsazSettings} from "../../appsazSettings";
import {TopsellersService} from "../../providers/service/topsellers-service";
import {LocalCartPage} from "../local-cart/local-cart";

/**
 * Generated class for the TopsellersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-topsellers',
  templateUrl: 'topsellers.html',
})
export class TopsellersPage {

    has_more_topsellers_items: boolean = true;
    topsellers_loading: boolean = true;


    constructor(public appsazSettings: AppsazSettings,
                private network: Network  ,
                public params: NavParams ,
                public toastCtrl: ToastController,
                public nav: NavController,
                public service: Service,
                public values: Values,
                public topsellerService :TopsellersService,
                ) {

        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }


        this.topsellerService.getTopsellers().then(sucess => {
                this.topsellers_loading = false;
            },
            failure => {
            });

    }


    getCart() {
        this.nav.push(LocalCartPage);
    }


    doInfinite_topsellers(infiniteScroll){
        this.topsellerService.loadMore_topsellers().then((results) => this.handleMoreTopsellers(results, infiniteScroll));
    }


    handleMoreTopsellers(results, infiniteScroll) {
        if (!results) {
            this.has_more_topsellers_items = false;
        }
        infiniteScroll.complete();
    }


    getProduct(item) {
        this.nav.push(ProductPage, item.id);
    }


}
