import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {AppsazSettings} from "../../appsazSettings";
import {ProductService} from "../../providers/service/product-service";
import {Service} from "../../providers/service/service";
import {Network} from "@ionic-native/network";
import {Functions} from "../../providers/service/functions";
import {Values} from "../../providers/service/values";
import {SocialSharing} from "@ionic-native/social-sharing";

/**
 * Generated class for the BlogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blog-detail.html',
})
export class BlogDetailPage {

  blog : any;
  loading : boolean = true;

  constructor(public appsazSettings: AppsazSettings,
              public nav: NavController,
              public service: ProductService,
              public publicService: Service,
              public params: NavParams,
              public functions: Functions,
              public values: Values,
              ) {

    this.blog = params.data;
    console.log("blog detail");
    console.log(this.blog);
    this.loading = false;
  }

}
