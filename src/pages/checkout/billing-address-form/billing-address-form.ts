import {Component} from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {CheckoutService} from '../../../providers/service/checkout-service';
import {Functions} from '../../../providers/service/functions';
import {Values} from '../../../providers/service/values';
import {Plugins} from "@capacitor/core";
import {LocalCartServiceProvider} from "../../../providers/service/local-cart-service";
import {Config} from "../../../providers/service/config";
import {Service} from "../../../providers/service/service";
import {LocalCartPage} from "../../local-cart/local-cart";
import {OrderDetails} from "../../account/order-details/order-details";
import {Home} from "../../home/home";
import {AppsazSettings} from "../../../appsazSettings";

const {Storage} = Plugins;
const {Devices} = Plugins;

@Component({
    templateUrl: 'billing-address-form.html'
})
export class BillingAddressForm {
    billingAddressForm: any;
    billing: any;
    regions: any;
    status: any;
    errorMessage: any;
    address: any;
    form: any;
    formLocal: any;
    states: any;
    orderReview: any;
    loginData: any;
    id: any;
    couponStatus: any;
    showCreateAccont: boolean = false;
    buttonSubmit: boolean = false;
    buttonSubmitLogin: boolean = false;
    buttonSubmitCoupon: boolean = false;
    buttonText: any;
    buttonText1: any;
    buttonText2: any;
    mydate: any;
    time: any;
    date: any;
    selectedDate: any;
    shipping: any;
    order: any;
    chosen_shipping: any;
    orderId: any;
    need_address: boolean = false;
    coupon: any = '';
    shippingLocal: any;
    private loading: any = false;

    deliveryDates: any = [];
    deliveryTimes: any = [];
    todayf: any = [];
    tommorowf: any = [];

    myIndex: number = 0;
    myIndexNew: number = 0;
    deliveryHours: any = [];

    private deliveryTimesLoading: boolean = true;
    OrderReviewLoading: boolean = false;


    constructor(public iab: InAppBrowser,
                public nav: NavController,
                private platform: Platform,
                public service: CheckoutService,
                params: NavParams,
                public functions: Functions,
                public values: Values,
                public localCartServiceProvider: LocalCartServiceProvider,
                public config: Config,
                public appsazSettings: AppsazSettings,
                public publicService: Service) {
        this.buttonText = "Place order";
        this.buttonText1 = "Apply";
        this.buttonText2 = "Login";
        this.loginData = [];
        this.formLocal = {};
        this.form = params.data.results;
        this.need_address = params.data.needAddress;

        if (params.data.coupon) {
            this.coupon = params.data.coupon;
        }

        this.form.shipping_method = [];
        this.billing = {};
        this.billing.save_in_address_book = true;
        // this.getRegion(this.form.billing_country);
        // this.getRegion(this.form.shipping_country);
        this.form.shipping = true;
        this.shipping = {};
        this.shipping.save_in_address_book = true;

        console.log('billing page');
        console.log(this.form);
        this.orderReview = this.form;
        // this.getRegion(this.form.billing_country);


        this.publicService.getDeliveryTimes().then(data => {
            console.log(data);
            /*  /!*for (let i = 1; i < 100 ; i++) {*!/
              for (let i = 0; i < 100 ; i++) {
                  if(data[i] != undefined){
                      this.deliveryTimes.push(data[i])
                      console.log(i)
                  }
                  else {
                      break;
                  }
              }*/
            let d = new Date();
            let n = d.getDay();
            console.log(" data.length", data.length)
            for (let i = 0; i < data.length; i++) {
                // if(data[(n % 7) + i] != undefined){
                this.deliveryTimes.push(data[(n + i) % 7])
                console.log(i)
                /*}
                else {
                    break;
                }*/
            }
            console.log("this.deliveryTimes", this.deliveryTimes)
            this.prepareDeliveryDatesTimes();
            this.deliveryTimesLoading = false;
            console.log(this.deliveryTimes);
        });

        this.form['billing_address_1'] = this.publicService.loggedInCustomer ['customer']['billing_address'].address_1;
        this.form['billing_address_2'] = this.publicService.loggedInCustomer ['customer']['billing_address'].address_2;
        this.form['billing_city'] = this.publicService.loggedInCustomer ['customer']['billing_address'].city;
        this.form['billing_state'] = this.publicService.loggedInCustomer ['customer']['billing_address'].state;
        this.form['billing_company'] = this.publicService.loggedInCustomer ['customer']['billing_address'].company;
        //this.form['billing_country'] = this.publicService.loggedInCustomer ['customer']['billing_address'].country;
        /*this.form['billing_country'] = 'Denmark';*/
        this.form['billing_country'] = 'DK';
        this.form['billing_first_name'] = this.publicService.loggedInCustomer ['customer']['billing_address'].first_name;
        this.form['billing_last_name'] = this.publicService.loggedInCustomer ['customer']['billing_address'].last_name;
        //this.form['billing_phone'] = this.publicService.loggedInCustomer ['customer']['billing_address'].phone;
        this.form['billing_phone'] = '';
        this.form['billing_postcode'] = this.publicService.loggedInCustomer ['customer']['billing_address'].postcode;

        var appname = this.appsazSettings.appNameEng.replace(" ", "");
        // this.form.billing_email = "appuser" + this.form.billing_phone + "@"+appname+".appsaz";
        this.form.billing_email = this.publicService.loggedInCustomer ['customer']['email'];


        this.form.payment_method = 'cod';
    }

    getRegion(countryId) {
        // this.states = this.form.state[countryId];
        // this.form.coupon = this.coupon;
        // this.service.updateOrderReview(this.form)
        //     .then((results) => this.handleOrderReviews(results));
    }

    handleOrderReviews(results) {
        console.log('order review: ');
        console.log(results);
        this.orderReview = results;
        this.form.payment = this.orderReview.payment;
        //this.chosen_shipping = this.OrderReview.chosen_shipping;
    }

    fillForm() {
        // this.form
        /*if (this.form.billing_first_name == '' || this.form.billing_first_name == null || !this.form.billing_first_name)
            this.form.billing_first_name = '-';
        if (this.form.billing_last_name == '' || this.form.billing_last_name == null || !this.form.billing_last_name)
            this.form.billing_last_name = '-';*/
        if (this.form.billing_company == '' || this.form.billing_company == null || !this.form.billing_company)
            this.form.billing_company = '-';
        if (this.form.billing_email == '' || this.form.billing_email == null || !this.form.billing_email)
            this.form.billing_email = 'temp@temp.com';
       /* if (this.form.billing_phone == '' || this.form.billing_phone == null || !this.form.billing_phone)
            this.form.billing_phone = '-';*/
      /*  if (this.form.billing_address_1 == '' || this.form.billing_address_1 == null || !this.form.billing_address_1)
            this.form.billing_address_1 = '-';*/
        if (this.form.billing_address_2 == '' || this.form.billing_address_2 == null || !this.form.billing_address_2)
            this.form.billing_address_2 = '-';
        /*if (this.form.billing_city == '' || this.form.billing_city == null || !this.form.billing_city)
            this.form.billing_city = '-';*/
       /* if (this.form.billing_postcode == '' || this.form.billing_postcode == null || !this.form.billing_postcode)
            this.form.billing_postcode = '000000';*/
        if (this.form.shipping_first_name == '' || this.form.shipping_first_name == null || !this.form.shipping_first_name)
            this.form.shipping_first_name = '-';
        if (this.form.shipping_last_name == '' || this.form.shipping_last_name == null || !this.form.shipping_last_name)
            this.form.shipping_last_name = '-';
        if (this.form.shipping_company == '' || this.form.shipping_company == null || !this.form.shipping_company)
            this.form.shipping_company = '-';
        if (this.form.shipping_email == '' || this.form.shipping_email == null || !this.form.shipping_email)
            this.form.shipping_email = 'temp@temp.com';
        if (this.form.shipping_phone == '' || this.form.shipping_phone == null || !this.form.shipping_phone)
            this.form.shipping_phone = '-';
        if (this.form.shipping_address_1 == '' || this.form.shipping_address_1 == null || !this.form.shipping_address_1)
            this.form.shipping_address_1 = '-';
        if (this.form.shipping_address_2 == '' || this.form.shipping_address_2 == null || !this.form.shipping_address_2)
            this.form.shipping_address_2 = '-';
        if (this.form.shipping_city == '' || this.form.shipping_city == null || !this.form.shipping_city)
            this.form.shipping_city = '-';
        if (this.form.shipping_postcode == '' || this.form.shipping_postcode == null || !this.form.shipping_postcode)
            this.form.shipping_postcode = '000000';
        if (this.form.billing_country == '' || this.form.billing_country == null || !this.form.billing_country)
            this.form.billing_country = 'IR';
        if (this.form.billing_state == '' || this.form.billing_state == null || !this.form.billing_state)
            this.form.billing_state = 'ADL';
        if (this.form.shipping_country == '' || this.form.shipping_country == null || !this.form.shipping_country)
            this.form.shipping_country = 'IR';
        if (this.form.shipping_state == '' || this.form.shipping_state == null || !this.form.shipping_state)
            this.form.shipping_state = 'ADL';

        // this.form.payment_method = '-';
        // this.form.checkout_nonce = '-';

        // if (form.password) {
        //     this.form.register = '-';
        //     this.form.password = '-';
        // }
        // if (form.onesignal_user_id)
        //     this.form.onesignal_user_id = '-';
    }

    checkout() {
        if (!this.form.payment_method)
            this.form.payment_method = 'cod';
        this.changePayment();
        //if (!this.need_address)
            //this.fillForm(); //dont fill form, check form and notif user

        this.buttonSubmit = true;
        this.buttonText = "Placing order...";

        if (this.platform.is('cordova'))
        // this.oneSignal.getIds().then((data: any) => {
        //     this.form.onesignal_user_id = data.userId;
        // });

        // if (this.form.shipping) {
            this.form.shipping_first_name = this.form.billing_first_name;
        this.form.shipping_last_name = this.form.billing_last_name;
        this.form.shipping_company = this.form.billing_company;
        this.form.shipping_address_1 = this.form.billing_address_1;
        this.form.shipping_address_2 = this.form.billing_address_2;
        this.form.shipping_city = this.form.billing_city;
        this.form.shipping_country = this.form.billing_country;
        this.form.shipping_state = this.form.billing_state;
        this.form.shipping_postcode = this.form.billing_postcode;

        this.shippingLocal = {
            'first_name': this.form.billing_first_name,
            'last_name': this.form.billing_last_name,
            'company': this.form.billing_company,
            'address_1': this.form.billing_address_1,
            'address_2': this.form.billing_address_2,
            'city': this.form.billing_city,
            'country': this.form.billing_country,
            'state': this.form.billing_state,
            'postcode': this.form.billing_postcode,
            'email': this.form.billing_email,
            'phone': this.form.billing_phone,
        };
        // }
        // if (this.form.payment_method == 'bacs' || this.form.payment_method == 'cheque' || this.form.payment_method == 'cod') {
        //     this.service.checkout(this.form)
        //         .then((results) => this.handleBilling(results));
        // } else

        if (this.checkFormContent()) {
            if (this.form.payment_method == 'payuindia') {
                this.service.checkout(this.form)
                    .then((results) => this.handlePayUPayment(results));
            } else if (this.form.payment_method == 'stripe') {
                // this.service.getStripeToken(this.form)
                //     .then((results) => this.handleStripeToken(results));
            } else {
                this.formLocal.customer_id = this.publicService.loggedInCustomer.customer.id;
                this.formLocal.payment_method = this.form.payment_gateways[0].id;
                this.formLocal.payment_method_title = this.form.payment_gateways[0].title;
                this.formLocal.set_paid = false;
                this.formLocal.status = 'on-hold';
                this.formLocal.billing = this.shippingLocal;
                this.formLocal.shipping = this.shippingLocal;
                this.formLocal.line_items = this.form.items;
                this.formLocal.currency = this.form.currency;
                this.formLocal.meta_data = [
                    {
                        key: 'deliveryDate',
                        value: this.form.daypart + ' - ' + this.form.timepart
                    }
                ];
                if (this.form.applied_coupons && this.form.applied_coupons.length > 0) {
                    this.formLocal.coupon_lines = [
                        {
                            code: this.form.applied_coupons[0]
                        }
                    ];
                }

                console.log('this.formLocal', this.formLocal);

                // this.formLocal.shipping_lines = [{
                //     "method_id": "free_shipping:2",
                //     "method_title": "حمل و نقل رایگان",
                //     "total": 0
                // }];
                // this.service.checkout(this.form)
                //     .then((results) => this.handlePayment(results));

                this.localCartServiceProvider.checkout(this.formLocal).then(result => {
                    console.log('ppppp', result);
                    this.orderId = result['id'];
                    // alert(this.orderId);
                    // if (result['total'] == 0 || result['total'] == '0') {
                    this.values.cart = [];
                    this.values.count = 0;


                    Storage.set({key: 'cart', value: ''});
                    Storage.remove({key: 'cart'});
                    this.localCartServiceProvider.emptyCart();

                    this.functions.showAlertMessage("", 'Your order submitted and Proceed successfully. You can check it at "Orders" in menu.');
                    // this.service.updateOrderStatus(this.orderId, "completed");
                    // this.service.updateOrderCustomerId(this.orderId, this.publicService.loggedInCustomer.customer.id);
                    this.nav.setRoot(Home);
                    // } else {
                    // let url = this.config.url + '/checkout/order-pay/' + result['id'] + '?key=' + result['order_key'] + '&in_app_payment=true';
                    // var options = "location=no,hidden=yes,toolbar=yes";
                    // let browser = this.iab.create(url, '_self', options);
                    // browser.on('loadstart').subscribe(data => {
                    //     console.log('zarinpal urllllll: ', data.url);
                    //     if (data.url.indexOf('wc_status=success') != -1) {
                    //         browser.hide();
                    //         this.functions.showAlertMessage("", 'سفارش شما با موفقیت ثبت و پرداخت شد و از قسمت "سفارشات پیشین" قابل پیگیری میباشد.');
                    //         this.values.cart = [];
                    //         this.values.count = 0;
                    //         Storage.set({key: 'cart', value: ''});
                    //         this.localCartServiceProvider.emptyCart();
                    //         let str = data.url;
                    //         let url_array = str.split("/");
                    //         let ind = 0;
                    //         let order_id = 0;
                    //         let order_received_index = url_array.filter(function (value, index) {
                    //             if (value == 'order-received') {
                    //                 ind = index;
                    //                 return true;
                    //             } else {
                    //                 return false;
                    //             }
                    //         });
                    //         this.orderId = url_array[ind + 1];
                    //
                    //         // const url = new URL(data.url);
                    //         // this.orderId = url.searchParams.get('order_id');
                    //         console.log("succes: " + this.orderId);
                    //         console.log("this.values.customerId: " + this.config.customer_id);
                    //         this.service.updateOrderStatus(this.orderId, "completed");
                    //         this.service.updateOrderCustomerId(this.orderId, this.config.customer_id);
                    //         this.nav.setRoot(Home);
                    //         browser.hide();
                    //
                    //     } else if (data.url.endsWith('checkout/') || data.url.endsWith('checkout') || data.url.endsWith('cart/')) {
                    //         browser.hide();
                    //         // const url = new URL(data.url);
                    //         // this.orderId = url.searchParams.get('order_id');
                    //         console.log("failed: " + this.orderId)
                    //         this.service.updateOrderStatus(this.orderId, "failed");
                    //         this.service.updateOrderCustomerId(this.orderId, this.config.customer_id);
                    //         this.functions.showAlertError("", 'متاسفانه پرداخت شما با مشکل مواجه شد. لطفا مجددا سفارش خود را ثبت کنید.');
                    //         // this.values.cart = [];
                    //         this.nav.pop();
                    //
                    //     }
                    //     // else if (data.url.indexOf('order-pay') != -1) {
                    //     //     console.log('dededede');
                    //     //     // browser.hide();
                    //     //     this.values.cart = [];
                    //     //     this.values.count = 0;
                    //     //     Storage.set({key: 'cart', value: ''});
                    //     //     let str = data.url;
                    //     //     let url_array = str.split("/");
                    //     //     let ind = 0;
                    //     //     console.log("order-pay: " + this.orderId);
                    //     //     console.log("this.values.customerId: " + this.values.customerId);
                    //     //     this.service.updateOrderStatus(this.orderId, "completed");
                    //     //     this.service.updateOrderCustomerId(this.orderId, this.values.customerId);
                    //     //     this.nav.setRoot(FirstPageMenuPage);
                    //     //     // browser.hide();
                    //     //
                    //     // }
                    // });
                    // browser.on('exit').subscribe(data => {
                    //     this.buttonSubmit = false;
                    // });
                    // browser.show();

                    // }
                }), error => {
                    this.buttonSubmit = false;
                };
            }
        }
    }


    handlePayUPayment(results) {
        var options = "location=no,hidden=yes,toolbar=no,hidespinner=yes";
        let browser = this.iab.create(results.redirect, '_blank', options);
        let str = results.redirect;
        var pos1 = str.lastIndexOf("/order-pay/");
        var pos2 = str.lastIndexOf("/?key=wc_order");
        var pos3 = pos2 - (pos1 + 11);
        this.orderId = str.substr(pos1 + 11, pos3);
        browser.on('loadstart').subscribe(data => {
            var browserActive = false;
            if (data.url.indexOf('payumoney.com/transact') != -1 && !browserActive) {
                browserActive = true;
                browser.show();
            } else if (data.url.indexOf('/order-received/') != -1 && data.url.indexOf('/?key=wc_order_') != -1) {
                if (this.orderId)
                    this.nav.push(OrderDetails, this.orderId);
                browser.hide();
            } else if (data.url.indexOf('cancel_order=true') != -1 || data.url.indexOf('cancelled=1') != -1 || data.url.indexOf('cancelled') != -1) {
                browser.close();
                this.buttonSubmit = false;
            }
        });
        browser.on('exit').subscribe(data => {
            this.buttonSubmit = false;
        });
    }

    handlePayment(results) {
        console.log("handlePayment results");
        console.log(results);
        if (results.result == 'success') {
            var options = "location=no,hidden=yes,toolbar=yes";
            let browser = this.iab.create(results.redirect, '_blank', options);

            browser.on('loadstart').subscribe(data => {
                console.log('zarinpal urllllllkkkkkk: ', data.url);
                if (data.url.indexOf('wc_status=success') != -1) {
                    browser.hide();
                    this.functions.showAlertMessage("", 'Your order submitted and Proceed successfully. You can check it at "Your Orders" in menu.');
                    this.values.cart = [];
                    this.values.count = 0;
                    Storage.set({key: 'cart', value: ''});
                    let str = data.url;
                    let url_array = str.split("/");
                    let ind = 0;
                    let order_id = 0;
                    let order_received_index = url_array.filter(function (value, index) {
                        if (value == 'order-received') {
                            ind = index;
                            return true;
                        } else {
                            return false;
                        }
                    });
                    this.orderId = url_array[ind + 1];

                    // const url = new URL(data.url);
                    // this.orderId = url.searchParams.get('order_id');
                    console.log("succes: " + this.orderId);
                    console.log("this.values.customerId: " + this.publicService.loggedInCustomer.customer.id);
                    this.service.updateOrderStatus(this.orderId, "completed");
                    this.service.updateOrderCustomerId(this.orderId, this.publicService.loggedInCustomer.customer.id);
                    this.nav.setRoot(Home);
                    browser.hide();

                } else if (data.url.endsWith('checkout/') || data.url.endsWith('checkout')) {
                    browser.hide();
                    // const url = new URL(data.url);
                    // this.orderId = url.searchParams.get('order_id');
                    console.log("failed: " + this.orderId)
                    this.service.updateOrderStatus(this.orderId, "failed");
                    this.service.updateOrderCustomerId(this.orderId, this.publicService.loggedInCustomer.customer.id);
                    this.functions.showAlertError("", 'Payment was not successful. Please submit your order again.');
                    // this.values.cart = [];
                    this.nav.pop();

                } else if (data.url.indexOf('order-received') != -1) {
                    console.log('dededede');
                    // browser.hide();
                    this.values.cart = [];
                    this.values.count = 0;
                    Storage.set({key: 'cart', value: ''});
                    let str = data.url;
                    let url_array = str.split("/");
                    let ind = 0;
                    let order_id = 0;
                    let order_received_index = url_array.filter(function (value, index) {
                        if (value == 'order-received') {
                            ind = index;
                            return true;
                        } else {
                            return false;
                        }
                    });
                    this.orderId = url_array[ind + 1];

                    // const url = new URL(data.url);
                    // this.orderId = url.searchParams.get('order_id');
                    console.log("succes: " + this.orderId);
                    console.log("this.values.customerId: " + this.publicService.loggedInCustomer.customer.id);
                    this.service.updateOrderStatus(this.orderId, "completed");
                    this.service.updateOrderCustomerId(this.orderId, this.publicService.loggedInCustomer.customer.id);
                    this.nav.setRoot(Home);
                    // browser.hide();

                }
            });
            browser.on('exit').subscribe(data => {
                this.buttonSubmit = false;
            });
            browser.show();
        } else if (results.result == 'failure') {
            this.functions.showAlertError("", results.messages);
            // this.service.updateOrderStatus(this.orderId, "failed");
            this.service.updateOrderCustomerId(this.orderId, this.publicService.loggedInCustomer.customer.id);
            this.buttonSubmit = false;
        }
    }

    checkoutStripe() {
        this.buttonSubmit = true;
        this.buttonText = "Placing Order...";
        // this.service.getStripeToken(this.form)
        //     .then((results) => this.handleStripeToken(results));
    }

    handleStripeToken(results) {
        if (results.error) {
            this.buttonSubmit = false;
            this.buttonText = "Place Order";
            this.functions.showAlertError("ERROR", results.error.message);
        } else {
            this.service.stripePlaceOrder(this.form, results)
                .then((results) => this.handleBilling(results));
        }
    }

    handleBilling(results) {
        if (results.result == "success") {
            var str = results.redirect;
            var pos1 = str.lastIndexOf("/order-received/");
            var pos2 = str.lastIndexOf("/?key=wc_order");
            var pos3 = pos2 - (pos1 + 16);
            var order_id = str.substr(pos1 + 16, pos3);
            this.orderId = order_id;
            this.values.cart = [];
            this.values.count = 0;
            this.orderId = order_id;
            this.nav.push(OrderDetails, order_id);
        } else if (results.result == "failure") {
            this.functions.showAlertError("ERROR", results.messages);
        }
        this.buttonSubmit = false;
        this.buttonText = "Place Order";
    }

    login() {
        if (this.validateForm()) {
            this.buttonSubmitLogin = true;
            this.buttonText2 = "Loading";
            this.service.login(this.form)
                .then((results) => this.handleResults(results));
        }
    }

    validateForm() {
        if (this.form.username == undefined || this.form.username == "") {
            return false
        }
        return !(this.form.password == undefined || this.form.password == "");
    }

    handleResults(a) {
        this.buttonSubmitLogin = false;
        this.buttonText2 = "Login";
        //this.form.shipping = true;
        if (a.user_logged) {
            this.form = a;
            this.states = this.form.state[this.form.billing_country];
            this.values.isLoggedIn = a.user_logged;
            // this.values.customerName = a.billing_first_name;
            this.config.customer_id = a.user_id;
            this.publicService.loggedInCustomer.customer.id = a.user_id;
            this.values.logoutUrl = a.logout_url;
        } else {
            this.functions.showAlertError("Error", 'Login unsuccessful. try again');
        }
    }

    submitCoupon() {
        this.buttonSubmitCoupon = true;
        this.buttonText1 = "Loading";
        this.service.submitCoupon(this.form)
            .then((results) => this.handleCoupon(results));
    }

    handleCoupon(results) {
        this.buttonSubmitCoupon = false;
        this.buttonText1 = "Apply";
        this.couponStatus = results._body;
        this.functions.showAlertMessage("STATUS", results._body);
        // this.service.updateOrderReview(this.form)
        //     .then((results) => this.OrderReview = results);
    }

    createAccount() {
        this.showCreateAccont = true;
    }

    changePayment() {
        this.form.payment_instructions = this.orderReview.payment_gateways[0].description;
        this.buttonSubmit = false;
        this.buttonText = "Continue to " + this.orderReview.payment_gateways[0].title;
    }

    updateOrderReview() {
        this.orderReview.shipping_methods.forEach((item, index) => {
            this.form.shipping_method[index] = item.chosen_method;
        })
        // this.service.updateOrderReview(this.form)
        //     .then((results) => this.handleOrderReviews(results));
    }

    deliveryTimeChange(selectedValue: any) {
        // console.log(selectedValue)
        // this.form['timepart'] = selectedValue;
    }


    deliveryDateChange(selectedValue: any) {
        this.deliveryHours = [];
        this.form.timepart = '';
        console.log("i: ", this.form.daypart)
        setTimeout(() => {
            console.log("index: ", this.myIndex);
            console.log("hours", this.deliveryTimes[this.myIndex].day_period)
            console.log("this.deliveryTimes[this.myIndex].day_period.length", this.deliveryTimes[this.myIndex].day_period.length)
            var j = 0;
            for (var i = 0; i < this.deliveryTimes[this.myIndex].day_period.length; i++) {
                //this.deliveryHours.push(this.deliveryTimes[this.myIndex].day_period[i])
                if (i != (this.deliveryTimes[this.myIndex].day_period.length) && this.deliveryTimes[this.myIndex].day_period[i].length > 0) {
                    this.deliveryHours[j] = {};
                    console.log("i", i)
                    this.deliveryHours[j]['period'] = (this.deliveryTimes[this.myIndex].day_period[i])
                    this.deliveryHours[j]['count'] = (this.deliveryTimes[this.myIndex].post_count[i])
                    j++;
                }
            }
            //this.form.dayIndex= this.myIndex;
            const today = new Date();
            const day1 = today.getDay();
            this.form.dayIndex = ((day1) + (this.myIndex)) % 7;
            console.log("this.form.dayIndex", this.form.dayIndex)
        }, 500)


        // let MomentDate = moment(selectedValue.date, 'YYYY/MM/DD');
        // MomentDate.locale('fa').format('YYYY/M/D');
        //
        //
        // this.form['daypart'] = MomentDate.locale('fa').format('YYYY/M/D') +" " +selectedValue.day;
        //
        // console.log(MomentDate.locale('fa').format('YYYY/M/D') +" " +selectedValue.day)
    }


    onChangeSelect(i) {
        console.log("ii: ", i)
        this.myIndex = i;
        const today = new Date();
        const day1 = today.getDay();
        this.myIndexNew = parseInt(day1 + i) % 7;
        console.log("newIndex", parseInt(day1 + i) % 7)
    }

    prepareDeliveryDatesTimes() {
        var dd: any;
        var mm: any   //As January is 0.
        var yyyy: any;
        var sp = "-";

        for (var i = 0; i < this.deliveryTimes.length; i++) {
            var today = new Date();
            dd = today.getDate();
            var day = new Date();
            day.setDate(today.getDate() + i);
            dd = day.getDate();
            mm = day.getMonth() + 1; //As January is 0.
            yyyy = day.getFullYear();
            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;
            this.tommorowf = [];
            this.tommorowf['date'] = yyyy + sp + mm + sp + dd;
            this.tommorowf['day'] = this.getDayName(day.getUTCDay());
            this.deliveryDates.push(this.tommorowf);
        }

        /* var today = new Date();
         dd = today.getDate();
         mm = today.getMonth()+1; //As January is 0.
         yyyy = today.getFullYear();
         if(dd<10) dd='0'+dd;
         if(mm<10) mm='0'+mm;
         this.todayf['date'] =yyyy+sp+mm+sp+dd;
         this.todayf['day'] = this.getDayName(today.getUTCDay());
         this.deliveryDates.push(this.todayf);

         var tomorrow = new Date();
         tomorrow.setDate(today.getDate()+1);
         dd = tomorrow.getDate();
         mm = tomorrow.getMonth()+1; //As January is 0.
         if(dd<10) dd='0'+dd;
         if(mm<10) mm='0'+mm;
         this.tommorowf['date'] =yyyy+sp+mm+sp+dd;
         this.tommorowf['day'] = this.getDayName(tomorrow.getUTCDay());
         this.deliveryDates.push(this.tommorowf);*/
        console.log(this.deliveryDates);
    }


    getDayName(dayNum) {
        var dayName;
        switch (dayNum) {
            case 0: {
                /*dayName = "یکشنبه";*/
                dayName = "Sunday";
                break;
            }
            case 1: {
                /*dayName = "دوشنبه";*/
                dayName = "Monday";
                break;
            }
            case 2: {
                /*dayName = "سه شنبه";*/
                dayName = "Tuesday";
                break;
            }
            case 3: {
                //dayName = "چهارشنبه";
                dayName = "Wednesday";
                break;
            }
            case 4: {
                dayName = "Thursday";
                /*dayName = "پنجشنبه";*/
                break;
            }
            case 5: {
                /*dayName = "جمعه";*/
                dayName = "Friday";
                break;
            }
            case 6: {
                /*dayName = "شنبه";*/
                dayName = "Saturday";
                break;
            }
        }
        return dayName;
    }





    checkFormContent(){
        var errorMsg = "";
        var hasError = false;
        if(this.form.billing_first_name.length == 0){
            errorMsg =  "First Name ";
            hasError = true;
        }
        else if(this.form.billing_last_name.length == 0){
            errorMsg =  "Last Name ";
            hasError = true;
        }
        else if(this.form.billing_phone.length == 0){
            errorMsg =  "Phone ";
            hasError = true;
        }
        else if(this.form.billing_city.length == 0){
            errorMsg = "City  ";
            hasError = true;
        }
        else if(this.form.billing_address_1.length == 0){
            errorMsg =  "Address  ";
            hasError = true;
        }
        else  if(this.form.billing_postcode.length == 0){
            errorMsg = "Post code  ";
            hasError = true;
        }
        else if(this.chosen_shipping && this.chosen_shipping.length == 0){
            errorMsg =  "Shipping method  ";
            hasError = true;
        }

        if(hasError){
            var message;
            message = "Please enter ";
            message = message + errorMsg ;
            message = message + " . " ;
            this.buttonSubmit = false;

            this.functions.showAlertWarning("",message);
            return false;
        }
        else if(!this.form.payment_method){
        }

        else if(!this.form.daypart || this.form.daypart=='' || !this.form.timepart || this.form.timepart==''){
            this.functions.showAlertWarning("","Select delivery time.");
            this.buttonSubmit = false;
        }

        else{
            return true;
        }

    }


}

