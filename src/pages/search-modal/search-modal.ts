import {Component, OnInit, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ProductPage} from "../product/product";
import {Searchbar} from 'ionic-angular';
import {GlobalSearchService} from "../../providers/service/global-serach-service";
import {AppsazSettings} from "../../appsazSettings";


/**
 * Generated class for the SearchModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-search-modal',
    templateUrl: 'search-modal.html',
})

export class SearchModalPage implements OnInit {


    @ViewChild('searchbar') searchbar: Searchbar;


    products: any;
    has_more_items: boolean = true;

    constructor(public navCtrl: NavController,
                public appsazSettings: AppsazSettings,
                public navParams: NavParams,
                public searchService: GlobalSearchService,
                public viewCtrl: ViewController
    ) {

        this.searchService.globalFilter.search = '';
        this.searchService.globalFilter.page = 1;

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SearchModalPage');
        setTimeout(() => {
            this.searchbar.setFocus();
        }, 50);

    }

    ngOnInit(): void {
        this.searchService.globalFilter.page = 1;
        this.searchService.search(this.searchService.globalFilter)
            .then((results) => this.handleSearchResults(results));
    }

    onInput($event) {
        console.log('onInput');
        this.searchService.globalFilter.page = 1;
        this.has_more_items = true;
        this.searchService.globalFilter.search = $event.srcElement.value;
        this.searchService.search(this.searchService.globalFilter).then((results) =>
            this.handleSearchResults(results));

    }

    handleSearchResults(results) {
        console.log(results)

        setTimeout(() => {
            this.searchbar.setFocus();
        }, 1);

        this.products = results;
    }

    onCancel($event) {
        console.log('cancelled');
    }


    getProduct(item) {
        this.navCtrl.push(ProductPage, item.id);
    }


    doInfinite(infiniteScroll) {
        this.searchService.globalFilter.page += 1;
        this.searchService.loadMore(this.searchService.globalFilter).then((results) => this.handleMore(results, infiniteScroll));
    }

    handleMore(results, infiniteScroll) {
        if (results != undefined) {
            for (var i = 0; i < results.length; i++) {
                this.products.push(results[i]);
            }
        }
        if (results.length == 0) {
            this.has_more_items = false;
        }
        infiniteScroll.complete();
    }

    closeModal() {
        this.viewCtrl.dismiss(false);
    }
}
