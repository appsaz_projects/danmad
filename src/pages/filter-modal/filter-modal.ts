import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, Events} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {CategoryService} from "../../providers/service/category-service";
import {Values} from "../../providers/service/values";

/**
 * Generated class for the FilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-filter-modal',
    templateUrl: 'filter-modal.html',
})
export class FilterModalPage implements OnInit {
    filter_types: Array<Object> = [];
    filter_terms: Array<Object> = [];
    filter_checked_terms: Array<Object> = [];
    filter_checked: Array<Object> = [];
    modalParams: Array<Object> = [];
    selectedFilter: number = 0;
    noFilter: boolean = true;
    selectedCategory: any;
    order: any;
    onSale: boolean;
    inStock: boolean;
    orderby: any;
    filter_onSale : boolean = false;
    filter_inStock : boolean = false;
    knobValues: any ={};
    rangeDefualt: any;
    sort: any;
    private products;
    private showFilters;
    private has_more_items;
    filter: any ={};


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public catService: CategoryService,
                public service: Service,
                public viewCtrl: ViewController,
                public values: Values,
                public events: Events) {

        this.knobValues.lower= this.catService.globalFilter['min_price'] ? this.catService.globalFilter['min_price']: this.values.data.min_price;
        this.knobValues.upper= this.catService.globalFilter['max_price'] ? this.catService.globalFilter['max_price'] : this.values.data.max_price;

    }

    ngOnInit(): void {
        if (!this.service.filter_types.length) {

            this.service.getAttributes().then(data => {
                this.noFilter = false;
                for (var i = 0; i < data.length; i++) {
                    this.filter_types.push({
                        id: data[i].id,
                        name: data[i].name,
                        slug: data[i].slug,
                    });
                    this.getAttributeVars(this.filter_types[0]);
                    this.selectedFilter = this.filter_types[0]["id"];

                }//for

            }, err => {
                console.log('error getting filters');
                console.log(err);
                this.noFilter = true;
            })
        }
        else {

            this.noFilter = false;
            this.filter_types=this.service.filter_types;
            this.selectedFilter = this.filter_types[0]["id"];
            this.getAttributeVars(this.filter_types[0]);
        }

    }

    getAttributeVars(attr: any): Promise<any> {
        this.selectedFilter = attr.id;
        if (!attr.terms) {
            return new Promise((resolve, reject) => {
                this.service.getAttributeVar(attr.id).then(data => {
                    console.log("getAttributeVars")
                    console.log(data)
                        this.filter_terms = [];
                        for (var i = 0; i < data.length; i++) {
                            this.filter_terms.push({
                                id: data[i].id,
                                name: data[i].name,
                                slug: data[i].slug,
                                parent_slug: attr.slug,
                                checked: false
                            });

                        }

                        for (let i = 0; i < this.filter_types.length; i++) {
                            if (this.filter_types[i]['id'] == attr.id) {
                                this.filter_types[i]['terms'] = this.filter_terms;
                            }
                        }

                        resolve(this.filter_terms);
                    },
                    error => {

                        console.log(error);
                        reject(false);
                    });
            });

        }
        else {
            for (let i = 0; i < this.filter_types.length; i++) {
                if (this.filter_types[i]['id'] == attr.id) {
                    this.filter_terms = this.filter_types[i]['terms'];
                }
            }
        }
    }

    getProductsByAttribute() {
        this.catService.load(this.catService.globalFilter).then(data => {
            /*this.viewCtrl.dismiss(data);*/
            this.viewCtrl.dismiss({data: data});
        }, err => {
        });

    }


    closeModal() {
        this.viewCtrl.dismiss(false);
    }

    addTerm(term: Object) {

        if (term['checked']) {
            console.log("is checked");
            for (let i = 0; i < this.filter_types.length; i++) {
                if (this.filter_types[i]['terms']) {
                    for (let j = 0; j < this.filter_types[i]['terms'].length; j++) {
                        if (term['id'] == this.filter_types[i]['terms'][j]['id']) {
                            this.filter_types[i]['terms'][j]['checked'] = false;
                            console.log(term);
                        }
                    }
                }
            }
        }
        else {
            console.log("is not checked");
            for (let i = 0; i < this.filter_types.length; i++) {
                if (this.filter_types[i]['terms']) {
                    for (let j = 0; j < this.filter_types[i]['terms'].length; j++) {
                        if (term['id'] == this.filter_types[i]['terms'][j]['id']) {
                            this.filter_types[i]['terms'][j]['checked'] = true;
                            console.log(term);
                        }
                    }
                }
            }
        }
    }

    showAttributes(filter) {
        this.selectedFilter = filter.id;
    }

    applyFilter() {

        this.clearGlobalFilter();

        this.filter_checked = [];
        this.filter_checked_terms = [];

      /*  for (let i = 0; i < this.filter_types.length; i++) {
            if (this.filter_types[i]['terms']) {
                this.filter_checked.push(this.filter_types[i]['slug']);
                for (let j = 0; j < this.filter_types[i]['terms'].length; j++) {
                    if (this.filter_types[i]['terms'][j]['checked']) {
                        this.filter_checked_terms.push(this.filter_types[i]['terms'][j]);
                    }
                }
            }
        }


        for (let i = 0; i < this.filter_checked_terms.length; i++) {

            this.catService.globalFilter['attributes' + i] = this.filter_checked_terms[i]['parent_slug'];
            this.catService.globalFilter['attribute_term' + i] = this.filter_checked_terms[i]['id'];

        }*/

        if(this.filter_inStock){
            this.catService.globalFilter['stock_status'] = 'instock';

        }
        if(this.filter_onSale){
            this.catService.globalFilter['on_sale'] = true;

        }

        //this.filter.page = 1;
        this.catService.globalFilter['page'] = 1;
        this.catService.globalFilter['min_price'] = this.knobValues.lower;
        this.catService.globalFilter['max_price'] = this.knobValues.upper;
        this.products = null;

        this.getProductsByAttribute();

        this.service.filter_types = this.filter_types;
    }

    clearGlobalFilter() {

        console.log("clearGlobalFilter" ,this.catService.globalFilter )

        var tempCat = this.catService.globalFilter['category'];
        var tempOrder = this.catService.globalFilter['order'];
        var tempOrderBy = this.catService.globalFilter['orderby'];
        var tempSearch= this.catService.globalFilter['search'];

        this.catService.globalFilter = [];
        this.catService.globalFilter['category'] = tempCat;
        this.catService.globalFilter['order'] = tempOrder;
        this.catService.globalFilter['orderby'] = tempOrderBy;
        this.catService.globalFilter['search'] = tempSearch;

    }


    clearFilters() {

        this.clearGlobalFilter();
        this.knobValues = {lower: 0, upper: this.values.data.max_price};

        this.filter_types = [];
        this.service.filter_types = [];

        this.service.getAttributes().then(data => {

            for (var i = 0; i < data.length; i++) {
                if (data[i].slug == 'pa_brand') {
                }
                else {
                    this.filter_types.push({
                        id: data[i].id,
                        name: data[i].name,
                        slug: data[i].slug,
                    });

                    this.noFilter = false;
                    this.getAttributeVars(this.filter_types[0]);
                    this.selectedFilter = this.filter_types[0]["id"];
                }

            }//for

            this.service.filter_types = this.filter_types;

        }, err => {
            console.log('error getting filters');
            console.log(err);
            this.noFilter = true;
        })

    }


    changePriceRange(event){
        this.rangeDefualt= event.value;
        console.log("this.rangeDefualt: ", this.rangeDefualt)
        console.log("min_price" , this.knobValues.lower)
        console.log("max_price" , this.knobValues.upper)
        //this.filter['max_price'] = this.knobValues.upper;
    }


    chnageFilterbetweenPrice(sort) {
        this.products = null;
        this.showFilters = false;
        this.has_more_items = true;
        this.filter.page = 1;
        this.filter['min_price'] = this.knobValues.lower;
        this.filter['max_price'] = this.knobValues.upper;

        this.catService.globalFilter['min_price'] = this.knobValues.lower;
        this.catService.globalFilter['max_price'] = this.knobValues.upper;
        this.getProductsByAttribute();

/*
        this.catService.load(this.filter)
            .then(results => (this.products = results));*/
    }

}
