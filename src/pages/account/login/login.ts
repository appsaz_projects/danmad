import {Component} from '@angular/core';
import {NavController, Platform, AlertController} from 'ionic-angular';
import {Service} from '../../../providers/service/service';
import {Functions} from '../../../providers/service/functions';
import {Values} from '../../../providers/service/values';
import {EditInfoPage} from "../edit-info/edit-info";
import {ActivationCodePage} from "../activation-code/activation-code";
import {CartPage} from "../../cart/cart";
import {Home} from "../../home/home";
import {AppsazSettings} from "../../../appsazSettings";
import {AccountsService} from "../../../providers/service/accounts-service";
import { GooglePlus } from '@ionic-native/google-plus';
import {LocalCartPage} from "../../local-cart/local-cart";


@Component({
    templateUrl: 'login.html',
    selector: 'login'
})


export class AccountLogin {
    loginData: any;
    loadLogin: any;
    status: any;
    error: any;
    nonce: any;
    public disableSubmit: boolean = false;
    customerData : any;
    customerData_loading : boolean=true;
    disableBtn: boolean = false;


    constructor(public appsazSettings : AppsazSettings ,
                public nav: NavController,
                public service: Service,
                public functions: Functions,
                public accountsService : AccountsService,
                public values: Values,
                public platform: Platform,
                private googlePlus: GooglePlus,
                public alertCtrl: AlertController) {
        this.loginData = {};
        this.service.getNonce()
            .then((results) => this.nonce = results);

        if(this.values.isLoggedIn){
            console.log("this.values.isLoggedIn")
            this.accountsService.getCustomerById(this.service.loggedInCustomer.customer.id.toString()).then(data =>{
                this.customerData=data;
                this.customerData_loading=false;
                console.log(this.customerData);
                console.log(this.customerData['customer']);

            });
        }
    }

    login() {
        if (this.validateForm()) {
            this.disableSubmit = true;
            this.loginData.googlePlusLogin= false;
            this.accountsService.login(this.loginData)
                .then((results) => {
                    this.handleResults(results);

                });
        }

    }


    getCart() {
        this.nav.push(LocalCartPage);
    }
    validateForm() {
        if (this.loginData.username == undefined || this.loginData.username == "") {
            return false
        }
        if (this.loginData.password == undefined || this.loginData.password == "") {
            return false
        } else {
            return true
        }
    }

    handleResults(results) {
        this.disableSubmit = false;
        this.disableBtn= false;
        if (!results.errors) {
            setTimeout(() => {
                //this.nav.pop();
                this.nav.setRoot(Home);
            }, 3000);
        } else if (results.errors) {
            /*this.functions.showAlertError('', 'نام کاربری یا رمز عبور اشتباه است.');*/
            if(results.errors.invalid_email){
                //this.functions.showAlertError('', 'Invalid email Unknown email address.');
                let alert = this.alertCtrl.create({
                    subTitle: 'Invalid email. Please register first or change email.',
                    buttons: [
                        {
                            text: 'Change email',
                            handler: () => {
                                return;
                            }
                        },
                        {
                            text: 'Register',
                            handler: () => {
                                this.nav.push(ActivationCodePage, {"action" : "register"});
                            }
                        }
                    ]
                });
                alert.present();
            }
            else{
                this.functions.showAlertError('', 'Username or password is not correct.');
            }

        }
    }

    forgotten() {
        if(this.appsazSettings.demo){
            /*this.functions.showAlertError("","این قابلیت در نسخه آزمایشی فروشگاه وجود ندارد")*/
            this.functions.showAlertError("","It is not possible in current version.")
        }
        else{
            this.nav.push(ActivationCodePage , {"action" : "forgetPass"});
        }
    }

    register() {
        this.nav.push(ActivationCodePage, {"action" : "register"});
    }

    editInfo() {
        this.nav.push(EditInfoPage);
    }



    subscribeMailpoet(email) {
        this.disableSubmit = true;
        this.accountsService.subscribeMailpoet(email)
            .then((results) => {
                this.disableSubmit = false;
                if (results.success && results.success == true) {
                    this.functions.showAlertSuccess('', results.data);
                } else if (!results.success || results.success == false) {
                    this.functions.showAlertError('', results.data);
                }
            });
    }


    googlePlusLogin(){
        this.disableBtn= true;
        this.googlePlus.disconnect().then(
            (msg) => {
                //alert("disconnect: "+ JSON.stringify(msg));
                this.loginGoogleFunction();
            }).catch(
            (msg) => {
                //alert("logout error: "+ JSON.stringify(msg));
                this.loginGoogleFunction();
            })

    }


    loginGoogleFunction(){
        this.googlePlus.login({})
            .then(res =>{
                    //alert("res"+ JSON.stringify(res))
                    this.loginData.username= res.email;
                    this.loginData.password= 'googlePlusLogin';
                    this.loginData.googlePlusLogin= true;
                    this.accountsService.login(this.loginData)
                        .then((results) => {
                            //alert("res"+ JSON.stringify(results))
                            this.handleResults(results);
                        });
                    //console.log("res", res);
                }
            )
            .catch(err =>{
                    this.disableBtn= false;
                    //console.error(err)
                    //alert("errorr "+ err)
                    /* this.loginData.googlePlusLogin= true;
                     this.loginData.username= 'email31@gmail.com';
                     this.loginData.password= 'googlePlusLogin';
                     //this.checkUserRegisterdBefore();
                     this.accountsService.login(this.loginData).then((results) => {
                     //alert("res"+ JSON.stringify(results))
                     this.handleResults(results);
                     });*/
                    //console.log("errorr ", err)
                }
            );
    }


    /*   checkUserRegisterdBefore(){
     this.accountsService.checkUserRegisterdBefore(this.loginData).then((results) => {
     alert("res"+ JSON.stringify(results))
     this.handleResults(results);
     });
     }*/



}
