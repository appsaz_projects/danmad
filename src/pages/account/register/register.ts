import { Component } from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import { Service } from '../../../providers/service/service';
import { Functions } from '../../../providers/service/functions';
import { Values } from '../../../providers/service/values';
import { Home } from '../../home/home';
import {AppsazSettings} from "../../../appsazSettings";
import {AccountsService} from "../../../providers/service/accounts-service";

@Component({
    templateUrl: 'register.html'
})
export class AccountRegister {
    registerData: any;
    countries: any;
    status: any;
    public disableSubmit: boolean = false;
    errors: any;
    loginStatus: any;
    country: any;
    billing_states: any;
    shipping_states: any;

    constructor(public nav: NavController,
                public service: Service,
                public functions: Functions,
                public values: Values,
                public platform: Platform ,
                public params : NavParams,
                public accountsService : AccountsService,
                public appsazSettings : AppsazSettings) {

        this.registerData = {};
        this.countries = {};
        this.registerData.billing_address = {};
        this.registerData.shipping_address = {};
        this.registerData.billing_address.phone = this.params.get("phoneNumber");
        //this.registerData.billing_address.country = "IR";
        this.registerData.billing_address.country = " ";

        this.service.getNonce()
            .then((results) => this.handleResults(results));


    }
    handleResults(results) {
        this.countries = results;
        this.getBillingRegion("IR");
    }
    getBillingRegion(countryId) {
        this.billing_states = this.countries.state[countryId];
    }
    getShippingRegion(countryId) {
        this.shipping_states = this.countries.state[countryId];
    }
    validateForm() {

        var appname = this.appsazSettings.appNameEng.replace(" ","");

        //this.registerData.email = "appuser"+this.registerData.billing_address.phone+"@"+appname+".appsaz";
        this.registerData.email= this.registerData.billing_address.phone;

        if (this.registerData.first_name == undefined || this.registerData.firstname == "") {
            /*this.functions.showAlertError("خطا", "لطفا نام خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your first name.");
            return false
        }
        if (this.registerData.last_name == undefined || this.registerData.lastname == "") {
            /*this.functions.showAlertError("خطا", "لطفا نام خانوادگی خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your last name.");
            return false
        }
        if (this.registerData.email == undefined || this.registerData.email == "") {
            /*this.functions.showAlertError("خطا", "لطفا ایمیل خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your email address.");
            return false
        }
        if (this.registerData.password == undefined || this.registerData.password == "") {
            /*this.functions.showAlertError("خطا", "لطفا پسورد خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your password.");
            return false
        }
        if (this.registerData.billing_address.phone == undefined || this.registerData.billing_address.phone == "") {
            /*this.functions.showAlertError("خطا", "شماره موبایل خود را وارد نکرده اید.");*/
            this.functions.showAlertError("Error", "Enter your phone number.");
            return false
        }
        this.registerData.username = this.registerData.email;
        this.registerData.billing_address.email = this.registerData.email;
        this.registerData.billing_address.first_name = this.registerData.first_name;
        this.registerData.billing_address.last_name = this.registerData.last_name;
        this.registerData.shipping_address.first_name = this.registerData.first_name;
        this.registerData.shipping_address.last_name = this.registerData.last_name;
        this.registerData.shipping_address.company = this.registerData.billing_address.company;
        this.registerData.shipping_address.address_1 = this.registerData.billing_address.address_1;
        this.registerData.shipping_address.address_2 = this.registerData.billing_address.address_2;
        this.registerData.shipping_address.city = this.registerData.billing_address.city;
        this.registerData.shipping_address.state = this.registerData.billing_address.state;
        this.registerData.shipping_address.postcode = this.registerData.billing_address.postcode;
        this.registerData.shipping_address.country = this.registerData.billing_address.country;
        return true;
    }
    registerCustomer() {
        console.log("registerData.billing_address.country");
        console.log(this.registerData.billing_address.country);

        this.errors = "";
        if (this.validateForm()) {
            this.disableSubmit = true;
            this.accountsService.registerCustomer(this.registerData)
                .then((results) => this.handleRegister(results));
        }
    }
    handleRegister(results) {
        console.log(results.errors);
        this.disableSubmit = false;
        if (!results.errors) {
            this.countries.checkout_login;
            this.accountsService.login(this.registerData)
                .then((results) => this.loginStatus = results);
            this.nav.setRoot(Home);
        }
        else if (results.errors) {
            this.errors = results.errors;
        }
    }
}
