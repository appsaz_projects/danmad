import {Component} from '@angular/core';
import {IonicPage, NavController, Platform} from 'ionic-angular';
import {Service} from "../../../providers/service/service";
import {Functions} from "../../../providers/service/functions";
import {Values} from "../../../providers/service/values";
import {AccountsService} from "../../../providers/service/accounts-service";

/**
 * Generated class for the EditInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-edit-info',
    templateUrl: 'edit-info.html',
})
export class EditInfoPage {

    public editData: any;
    countries: any;
    status: any;
    public disableSubmit: boolean = false;
    errors: any;
    loginStatus: any;
    country: any;
    billing_states: any;
    shipping_states: any;
    edit_loading: boolean = true;


    constructor(public nav: NavController,
                public service: Service,
                public functions: Functions,
                public values: Values,
                public accountsService : AccountsService,
                public platform: Platform,
                ) {


        this.countries = {};
        this.service.getNonce()
            .then((results) => this.handleResults(results));

        this.accountsService.getCustomerById(this.service.loggedInCustomer.customer.id.toString()).then(data => {
            this.editData = data;
            this.edit_loading = false;
        });


    }

    handleResults(results) {
        this.countries = results;

        this.getBillingRegion('IR')
    }

    getBillingRegion( countryId) {

        this.billing_states = this.countries.state[countryId];
    }

    getShippingRegion(countryId) {
        this.shipping_states = this.countries.state[countryId];
    }

    validateForm() {

        if (this.editData['customer'].billing_address.first_name == undefined || this.editData['customer'].billing_address.first_name == "") {
           /* this.functions.showAlertError("خطا", "لطفا نام خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your first name");
            return false
        }
        if (this.editData['customer'].billing_address.last_name == undefined || this.editData['customer'].billing_address.lastname == "") {
           /* this.functions.showAlertError("خطا", "لطفا نام خانوادگی خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your last name");
            return false
        }
        if (this.editData['customer'].billing_address.email == undefined || this.editData['customer'].billing_address.email == "") {
            /*this.functions.showAlertError("خطا", "لطفا ایمیل خود را وارد کنید.");*/
            this.functions.showAlertError("Error", "Enter your email address");
            return false
        }
        return true;
    }

    saveChanges() {

        this.errors = "";
        if (this.validateForm()) {
            this.disableSubmit = true;
            this.accountsService.updateCustomer(this.editData)
                .then((results) => this.handleEditInfo(results));
        }
    }


    handleEditInfo(results) {
        this.disableSubmit = false;
        if (!results.errors) {
            this.countries.checkout_login;
            this.accountsService.login(this.editData)
                .then((results) => this.loginStatus = results);

            /*this.functions.showToast('اطلاعات شما با موفقیت به روز رسانی شد.');*/
            this.functions.showToast('Your info updated successfully');

            this.nav.pop();
        }
        else if (results.errors) {
            this.errors = results.errors;
        }
    }


}
