import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {WishlistService} from '../../../providers/service/wishlist-service';
import {Values} from '../../../providers/service/values';
import {Functions} from '../../../providers/service/functions';
import {CartPage} from '../../cart/cart';
import {ProductPage} from '../../product/product';
import {Service} from "../../../providers/service/service";
import {OfflinePage} from "../../offline/offline";
import {Network} from "@ionic-native/network";
import {LocalCartPage} from "../../local-cart/local-cart";

@Component({
    templateUrl: 'wishlist.html'
})
export class WishlistPage {

    wishlist: any;
    error: any;
    wishlist_loading: boolean = true;
    noItems: boolean = false;

    constructor( private network: Network,
                 public nav: NavController,
                 public values: Values,
                 public publicService : Service,
                 public params: NavParams,
                 public functions: Functions,
                 public service: WishlistService) {


        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }

        this.service.loadWishlist()
            .then((results) => {
                this.wishlist = results;
                this.wishlist_loading = false;


                if(JSON.stringify(this.wishlist).length >0){

                    if(this.wishlist.error){
                        this.noItems=true;

                    }

                }

                else{
                    this.noItems=true;
                }


            });
    }

    removeFromWishlist(id) {
        this.service.deleteItem(id)
            .then((results) => this.updateWish(results, id));
    }

    updateWish(results, id) {
        if (results.status == "success") {
            this.wishlist_loading = true;
            this.service.loadWishlist()
                .then((results) =>{
                    this.wishlist = results;
                    this.wishlist_loading = false;

                    if(JSON.stringify(this.wishlist).length >0){

                        if(this.wishlist.error){
                            this.noItems=true;

                        }

                    }
                    else{
                        this.noItems=true;
                    }

                } );
            this.values.wishlistId.splice(id, 1);
        }
    }

    getCart() {
        this.nav.push(LocalCartPage);
    }

    addToCart(id, type) {
        if (type == 'variable') {
            this.nav.push(ProductPage, id);
        } else {
            this.service.addToCart(id)
                .then((results) => this.updateCart(results, id));
        }
    }

    updateCart(results, id) {
        if (results.error) {
            this.functions.showToast( "بروز خطا در افزودن به لیست علاقمندی ها");
        } else {

            this.updateWishlist(results);
        }
    }


    updateWishlist(results) {
        this.service.loadWishlist()
            .then((results) => this.wishlist = results);
        this.functions.showToast("محصول به لیست علاقمندی ها اضافه شد.");
    }

    getProduct(item) {
        console.log("item", item["value"].id);
        this.publicService.getProductById(item["value"].id).then(data=>{
            console.log("data", data)
            this.nav.push(ProductPage, data["product"].id);
        })

    }
}
