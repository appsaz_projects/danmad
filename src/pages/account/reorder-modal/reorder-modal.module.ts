import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReorderModalPage } from './reorder-modal';

@NgModule({
  declarations: [
    //ReorderModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ReorderModalPage),
  ],
})
export class ReorderModalPageModule {}
