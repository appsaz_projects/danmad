import {Component, ViewChild} from '@angular/core';
import {ModalController, NavController, NavParams, Slides, ToastController} from 'ionic-angular';
import {Service} from '../../providers/service/service';
import {Values} from '../../providers/service/values';
import {CartPage} from '../cart/cart';
import {ProductsPage} from '../products/products';
import {ProductPage} from '../product/product';
import {OnSaleListPage} from "../on-sale-list/on-sale-list";
import {TopsellersPage} from "../topsellers/topsellers";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {AppsazSettings} from "../../appsazSettings";
import {SearchModalPage} from "../search-modal/search-modal";
import {animate, style, transition, trigger} from "@angular/animations";
import {FeaturedListPage} from "../featured-list/featured-list";
import {CategoryService} from "../../providers/service/category-service";
import {TopsellersService} from "../../providers/service/topsellers-service";
import {OnsaleService} from "../../providers/service/onsale-service";
import {AccountsService} from "../../providers/service/accounts-service";
import {FeaturedService} from "../../providers/service/featured-service";
import {ProductService} from "../../providers/service/product-service";
import {BlogDetailPage} from "../blog-detail/blog-detail";
import {LocalCartPage} from "../local-cart/local-cart";

@Component({

    animations: [
        trigger(
            'myBlockAnimation',
            [
                transition(
                    ':enter', [
                        style({transform: 'translateX(100%)', opacity: 0}),
                        animate('500ms', style({transform: 'translateX(0)', 'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({transform: 'translateX(0)', 'opacity': 1}),
                        animate('500ms', style({transform: 'translateX(100%)', 'opacity': 0}))

                    ]
                )]
        ),
        trigger(
            'myBoxAnimation',
            [
                transition(
                    ':enter', [
                        style({opacity: 0}),
                        animate('500ms', style({'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({'opacity': 1}),
                        animate('500ms', style({'opacity': 0}))

                    ]
                )]
        )
    ],
    templateUrl: 'home.html'
})
export class Home {
    status: any;
    entered: boolean = false;
    items: any;
    product: any;
    options: any;
    id: any;
    variationID: any;
    time: any;
    has_more_items: boolean = true;
    has_more_onsale_items: boolean = true;
    loading: boolean = true;
    localSlides : any =[];
    searchModal: any;
    @ViewChild(Slides) private slides: Slides;



    constructor(public appsazSettings: AppsazSettings,
                public modalCtrl: ModalController,
                public params: NavParams,
                private iab: InAppBrowser,
                public toastCtrl: ToastController,
                public nav: NavController,
                public service: Service,
                public topsellerService :TopsellersService,
                public onsaleService : OnsaleService,
                public categoryService : CategoryService,
                public accountsService : AccountsService,
                public featuredService : FeaturedService,
                public values: Values,
                public productService: ProductService
                ) {

        if (this.service.isDeep.value > 0) {
            console.log(this.service.isDeep.value);
            this.nav.push(ProductPage, this.service.isDeep.value);
        }

        this.items = [];
        this.options = [];

        if(!this.appsazSettings.advanced){
        this.localSlides = ["assets/banners/ban1.png", "assets/banners/ban2.png","assets/banners/ban3.png"];

        }

    }

    getCategory(id, slug, name) {
        this.items.category = [];
        this.items.category.id = id;
        this.items.category.slug = slug;
        this.items.category.name = name;
        this.items.category.categories = this.categoryService.categories.filter(item => item.parent === parseInt(id));
        this.nav.push(ProductsPage, this.items);
    }


    getCart() {
        this.nav.push(LocalCartPage);
    }

    ionViewDidEnter() {
        // this.slides.dir = 'rtl';
        this.entered = true;
    }


    openSearch() {
        this.searchModal = this.modalCtrl.create(SearchModalPage);
        this.searchModal.onDidDismiss(data => {

        });

        this.searchModal.present();

    }


    doInfinite(infiniteScroll) {
        this.service.loadMore().then((results) => this.handleMore(results, infiniteScroll));
    }

    handleMore(results, infiniteScroll) {
        if (!results) {
            this.has_more_items = false;
        }
        infiniteScroll.complete();
    }

    getProduct(item) {
        this.nav.push(ProductPage, item.id);
    }

    featuredList() {
        this.nav.push(FeaturedListPage);
    }

    getSubCategories(id) {
        const results = this.categoryService.categories.filter(item => item.parent === parseInt(id));
        return results;
    }

    onsaleList() {
        this.nav.push(OnSaleListPage);
    }

    topsellersList() {
        this.nav.push(TopsellersPage);
    }


  /*  loadBannerLink(index: number) {
        console.log(this.service.homeSliders[index].description);
        // this.loadSite(this.service.homeSliders[index].description);
    }*/

    loadSite(url) {
        var target = "_system";

        try {
            const browser = this.iab.create(url, target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=بستن فرم');
        } catch (error) {
            console.log("inApp browser error");
        }
    }



   /* loadBannerLink(type , banner) {
        console.log(type)
        console.log(banner)

        switch (type) {
          /!*  case 'product_list':{
                console.log('product_list');
                this.nav.push(ProductsStaticListPage , banner);
                break;

            }*!/
            case 'product':{

                console.log('product');
                this.nav.push(ProductPage , banner.product);
                break;

            }
            case 'blog':{

                console.log('blog')
                this.nav.push(BlogDetailPage , banner.blog)

                break;

            }
            case 'link':{

                console.log('link');

                var target = "_system";
                var url = banner.link;

                try {
                    this.iab.create(url, target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=close');
                }
                catch (error) {
                    console.log("inApp browser error");
                }
                break;

            }
            case 'category':{
                console.log('category');
                this.getCategory( banner.category.term_id ,  banner.category.slug , banner.category.name);
                break;

            }
        }
    }*/


    loadBannerLink(type, banner) {
        console.log(type);
        console.log(banner);

        switch (type) {
           /* case 'product_list': {
                console.log('product_list');
                this.nav.push(ProductsStaticListPage, banner);
                break;
            }*/
            case 'product': {
                console.log('product');
                this.nav.push(ProductPage, banner.product);
                break;

            }
            /*case 'blog': {

                console.log('!!!!blog: ', banner.blog)
                this.nav.push(BlogDetailPage, banner.blog)
                break;

            }*/
            case 'link': {

                console.log('link');

                var target = "_system";
                var url = banner.link;

                try {
                    this.iab.create(url, target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=close');
                } catch (error) {
                    console.log("inApp browser error");
                }
                break;

            }
            case 'category': {

                console.log('category');

                this.getCategory(banner.category.term_id, banner.category.slug, banner.category.name);

                break;

            }

        }

    }



    addToCart(product, index) {
       // this.disableBtn= true;
        //this.selectedItemIndex= index;
         this.addToCartFunction(product);
    }


    addToCartFunction(product){
        console.log("111 ", product)
       /* this.options = [];
        this.options.product_id = product.id;
        this.productService.addToCart(this.options).then((results) => {
            this.updateCart(results);
        });*/
        //this.product = product;
        this.options = [];
        this.options.product_id = product.id;
        if (this.values.isLoggedIn) {
            if (product.stock_amount !=0) {
                //if (this.setVariations()) {
                    console.log(this.options)
                    this.productService.addToCart(this.options).then((results) => {
                        this.updateCart(results);
                    });
                //}
            }
        } else {
             //this.showLoginToast();
        }
    }

    updateCart(a) {
        if(!a.error){
            this.showCartToast();
            this.values.count += parseInt('1');
        }
        /* this.disableSubmit = false;
         this.addToCartLoading = false;*/
    }



    showCartToast() {
        let toast = this.toastCtrl.create({
            message: 'Added to cart.',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            closeButtonText: "view cart",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(LocalCartPage);
            }
        });
        toast.present();
    }


    

}
