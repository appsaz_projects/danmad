import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, AlertController, ToastController, ModalController, App} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {Home} from '../pages/home/home';
import {Service} from '../providers/service/service';
import {Values} from '../providers/service/values';
import {Config} from '../providers/service/config';
import {TranslateService} from '@ngx-translate/core';
import {ProductsPage} from '../pages/products/products';
import {AccountLogin} from '../pages/account/login/login';
import {Orders} from '../pages/account/orders/orders';
import {WishlistPage} from '../pages/account/wishlist/wishlist';
import {SocialSharing} from '@ionic-native/social-sharing';
import {NativeStorage} from '@ionic-native/native-storage';
import {AboutUsPage} from "../pages/about-us/about-us";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {Deeplinks} from "@ionic-native/deeplinks";
import {SplashPage} from "../pages/splash/splash";
import {Functions} from "../providers/service/functions";
import {OnSaleListPage} from "../pages/on-sale-list/on-sale-list";
import {TopsellersPage} from "../pages/topsellers/topsellers";
import {OfflinePage} from "../pages/offline/offline";
import {Network} from '@ionic-native/network';
import {Device} from '@ionic-native/device';
import {AppsazSettings} from "../appsazSettings";
import {NotifListPage} from "../pages/notif-list/notif-list";
import {FCM} from "@ionic-native/fcm";
import {notificationService} from "../providers/service/notification-service";
import {BlogsPage} from "../pages/blogs/blogs";
import {Market} from '@ionic-native/market';
import {ImgCacheService} from "../global/img-cache";
import {FeaturedListPage} from "../pages/featured-list/featured-list";
import {TopsellersService} from "../providers/service/topsellers-service";
import {OnsaleService} from "../providers/service/onsale-service";
import {CategoryService} from "../providers/service/category-service";
import {FeaturedService} from "../providers/service/featured-service";
import {AccountsService} from "../providers/service/accounts-service";
import {CartService} from "../providers/service/cart-service";
import {ActivationCodePage} from "../pages/account/activation-code/activation-code";
import {MySpecialItemsPage} from "../pages/account/my-special-items/my-special-items";
import {LocalCartServiceProvider} from "../providers/service/local-cart-service";
import {Plugins} from "@capacitor/core";
import {LocalCartPage} from "../pages/local-cart/local-cart";

const {Storage} = Plugins;

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav
    rootPage: any;
    androidVersion: any = 6;
    allowClose: any;
    lastBack: any;
    newMessagesCount: number = 0;
    items: any;

    constructor(private market: Market,
                public  imgCacheService: ImgCacheService,
                public app: App,
                public fcm: FCM,
                public statusBar: StatusBar,
                public notifService: notificationService,
                public alertCtrl: AlertController,
                public config: Config,
                public platform: Platform,
                public service: Service,
                public values: Values,
                public cartService: CartService,
                public translateService: TranslateService,
                private functions: Functions,
                private socialSharing: SocialSharing,
                public appsazSettings: AppsazSettings,
                private network: Network,
                private nativeStorage: NativeStorage,
                private device: Device,
                private toastCtrl: ToastController,
                private iab: InAppBrowser,
                public deeplinks: Deeplinks,
                private modalCtrl: ModalController,
                public topsellerService: TopsellersService,
                public onsaleService: OnsaleService,
                public categoryService: CategoryService,
                public accountsService: AccountsService,
                public localCartService: LocalCartServiceProvider,
                public featuredService: FeaturedService) {

        // handle back button

        platform.registerBackButtonAction(() => {
            // const overlay = app._appRoot._overlayPortal.getActive();
            const nav = app.getActiveNav();
            const closeDelay = 2000;
            const spamDelay = 500;
            /* if(overlay && overlay.dismiss) {
             overlay.dismiss();
             }*/
            let activePortal = this.app._appRoot._loadingPortal.getActive() ||
                this.app._appRoot._modalPortal.getActive() ||
                this.app._appRoot._toastPortal.getActive() ||
                this.app._appRoot._overlayPortal.getActive();
            if (activePortal && !this.allowClose) {
                var ready = false;
                activePortal.dismiss();
                activePortal.onDidDismiss(() => {
                    ready = true;
                });
                return;
            } else if (nav.canGoBack()) {
                nav.pop();
            } else if (Date.now() - this.lastBack > spamDelay && !this.allowClose) {
                this.allowClose = true;
                let toast = this.toastCtrl.create({
                    /*message: "برای خروج دکمه بازگشت را دوباره لمس کنید. ",*/
                    message: "To exit the app, press back one more time.",
                    duration: closeDelay,
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(() => {
                    this.allowClose = false;
                });
                toast.present();
            } else if (Date.now() - this.lastBack < closeDelay && this.allowClose) {
                this.platform.exitApp();
            }
            this.lastBack = Date.now();
        });


        platform.ready().then(() => {

            // choose statusbar style: styleDefault | styleLightContent

            this.translateService.setDefaultLang(this.values.settings.language);
            this.translateService.use(this.values.settings.language);

            // statusBar.styleDefault();
            statusBar.styleLightContent();
            statusBar.backgroundColorByHexString('#000000');


            if (this.network.type === 'none') {
                this.nav.setRoot(OfflinePage);
            } else {
                setTimeout(() => {
                    this.nav.setRoot(Home);

                    imgCacheService.initImgCache()
                        .subscribe((v) => console.log('init'), () => console.log('fail init'));

                }, 3000);
            }

            // load data
            this.categoryService.getCategories(1);
            this.onsaleService.getOnsale();
            this.topsellerService.getTopsellers();
            // this.service.getProducts();
            this.featuredService.getFeatured();
            this.updateCartCount();


            // features only available in advanced mode:

            /*   if(this.appsazSettings.advanced){

             //handle mandatory update

             this.service.checkForUpdates().then(data=>{

             var latestVersion : number ;

             if(this.platform.is("android")){
             latestVersion  = data["latestAndroidVersion"];

             }
             else if(this.platform.is("ios")){
             latestVersion  = data["latestIosVersion"];

             }
             else{

             latestVersion  = data["latestAndroidVersion"];
             }

             if(this.updateAvailable(latestVersion , this.appsazSettings.appVersion)) {
             if (data["mustUpdate"]){
             this.mustUpdateDialog();
             }
             else{

             this.updateAvailableDialog();
             }
             }


             }, error =>{

             });

             this.notifService.getNewMessages(0,100).then(data =>{
             this.newMessagesCount = data.length;
             });

             this.service.getBanners();
             }*/

            //this.service.getBanners();
            this.service.getLinkablebanners();


            //login process using capacitor

            Storage.get({key: 'loginData'}).then(data => {
                console.log("capacitor loginData")
                console.log(data)
                if ((data.value) != null && (data.value) != '{}') {
                    this.accountsService.login(data.value);
                } else {
                    this.accountsService.loginLoading = false;
                }
            }, error => {
                this.accountsService.loginLoading = false;
                console.log(error)
            });

            // show splash page
            let splash = this.modalCtrl.create(SplashPage);
            splash.present();


            //handle deeplinks:

            if (this.platform.is("cordova")) {

                this.deeplinks.routeWithNavController(this.nav, {}).subscribe(match => {

                    if (match['$link']) {

                        var idString = match['$link']['queryString'];
                        var id = idString.substr(3, idString.length - 3);

                        this.service.setIsDeep(id)

                    } else {
                        this.nav.setRoot(Home);
                    }
                }, nomatch => {

                    this.nav.setRoot(Home);

                });


            }


            if (this.platform.is("cordova")) {

                this.androidVersion = parseInt(this.device.version);
                if (this.androidVersion < 5.1) {
                    /*this.functions.showAlertWarning("", "کاربر گرامی، نسخه اندروید گوشی شما کمتر از 5.1 است. ممکن است اپلیکیشن از نظر کارکرد و ظاهر دچار اختلال شود.");*/
                    this.functions.showAlertWarning("", "Your phone android version is less than 5.1.1. It might affect on application");
                }


                //handle notofications
                if (this.appsazSettings.enableNotif) {
                    this.fcm.subscribeToTopic('all');

                    this.fcm.getToken().then(token => {
                        this.notifService.deviceRegistry(token);
                    });

                    this.fcm.onNotification().subscribe(data => {

                        if (data.wasTapped) {

                            this.notifService.setDelivered(data['msgId']);
                            this.functions.showAlertMessage(data.title, data.message)
                        } else {

                            this.notifService.setDelivered(data['msgId']);
                            this.notifService.setSeen(data['msgId']);
                            this.functions.showAlertMessage(data.title, data.message)
                        }
                        ;
                    });

                    this.fcm.onTokenRefresh().subscribe(token => {
                        this.notifService.deviceRegistry(token);
                    });
                }

            }

            this.service.load();

        });


    }


    logout() {

        let alert = this.alertCtrl.create({
            /*subTitle: 'از حساب کاربری خود خارج می شوید؟',*/
            subTitle: 'Want to logout? ',
            buttons: [
                {
                    /*text: ' بله',*/
                    text: ' Yes',
                    handler: () => {
                        this.accountsService.logout();
                        this.localCartService.emptyCart();
                        this.values.wishlistId = [];
                    }
                },
                {
                    /*text: 'خیر',*/
                    text: 'No',
                    handler: () => {

                    }
                }
            ]
        });
        alert.present();

    }

    mustUpdateDialog() {

        this.platform.registerBackButtonAction(() => {
            this.platform.exitApp();
        });

        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            /* subTitle: ' لطفا نسخه اپلیکیشن خود را به روز رسانی کنید. ',*/
            subTitle: ' Please update application. ',
            buttons: [
                {
                    /*text: 'به روز رسانی',*/
                    text: 'Update',
                    handler: () => {
                        this.market.open(this.appsazSettings.packageName);
                        this.platform.exitApp();
                    }
                },
                {
                    /* text: ' خروج از اپلیکیشن ',*/
                    text: ' Exit ',
                    handler: () => {
                        this.platform.exitApp();
                    }
                }

            ]
        });
        alert.present();
    }


    updateAvailableDialog() {
        let alert = this.alertCtrl.create({
            /*subTitle: 'نسخه جدید اپلیکیشن اکنون قابل دریافت است.  ',*/
            subTitle: 'New version is available.  ',
            cssClass: 'msg-class-update',
            buttons: [
                {
                    /*text: 'به روز رسانی',*/
                    text: 'Update now',
                    handler: () => {
                        this.market.open(this.appsazSettings.packageName);
                    }
                },
                {
                    /*text: ' بعدا ',*/
                    text: ' later ',
                    handler: () => {
                    }
                }
            ]
        });
        alert.present();
    }


    updateAvailable(latest, current) {

        var updateAvailable = false;

        var splitCurrent = current.split('.')
        var splitLatest = latest.split('.')


        if (splitLatest[0] > splitCurrent[0]) {
            updateAvailable = true;
            return updateAvailable;
        } else if (splitLatest[0] == splitCurrent[0]) {

            if (splitLatest[1] > splitCurrent[1]) {
                updateAvailable = true;
                return updateAvailable;
            } else if (splitLatest[1] == splitCurrent[1]) {

                if (splitLatest[2] > splitCurrent[2]) {
                    updateAvailable = true;
                    return updateAvailable;
                } else if (splitLatest[1] == splitCurrent[2]) {


                } else if (splitLatest[1] < splitCurrent[2]) {
                    updateAvailable = false;
                    return updateAvailable;
                }

            } else if (splitLatest[1] < splitCurrent[1]) {
                updateAvailable = false;
                return updateAvailable;
            }

        } else if (splitLatest[0] < splitCurrent[0]) {
            updateAvailable = false;
            return updateAvailable;
        }

        return updateAvailable;
    }

    showLoginToast() {
        let toast = this.toastCtrl.create({
            /* message: 'لطفا به حساب کاربری خود وارد شوید.',*/
            message: 'Please login',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            /*closeButtonText: "ورود",*/
            closeButtonText: "login",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(AccountLogin);
            }
        });
        toast.present();
    }

    updateCartCount() {
        this.cartService.loadCart().then(crt => {

                let cartItem = crt.cart_contents;
                let cart = [];
                let count = 0;
                for (let item in crt.cart_contents) {
                    this.cart[crt.cart_contents[item].product_id] = parseInt(crt.cart_contents[item].quantity);
                    count += parseInt(crt.cart_contents[item].quantity);
                }
                this.values.count = count;
                this.values.count = this.localCartService.localCart.length;

            },
            failure => {
            });
    }

    openCategories() {
        this.items = [];
        this.items.id = "0";
        this.items.slug = "0";
        this.items.name = "0";
        this.items.categories = "0";
        this.nav.push(ProductsPage, this.items);
    }

    getCart() {
        this.nav.push(LocalCartPage);
    }


    featuredList() {
        this.nav.push(FeaturedListPage);
    }

    login() {
        this.nav.push(AccountLogin);
    }

    order() {

        if (this.values.isLoggedIn) {
            this.nav.push(Orders);
        } else {

            this.showLoginToast();
        }
    }

    cart() {
        this.nav.push(LocalCartPage);
    }

    wishlist() {
        if (this.values.isLoggedIn) {

            this.nav.push(WishlistPage);
        } else {

            this.showLoginToast();
        }
    }

    openList() {
        this.nav.push(ProductsPage);
    }


    shareApp() {
        if (this.platform.is('cordova')) {
            var url = '';
            if (this.platform.is('android'))
                url = this.values.settings.share_app_android_link;
            else url = this.values.settings.share_app_ios_link;
            var options = {
                message: '',
                subject: '',
                files: ['', ''],
                url: url,
                chooserTitle: ''
            }
            this.socialSharing.shareWithOptions(options);
        }
    }

    aboutUs() {
        this.nav.push(AboutUsPage);
    }


    loadSite() {
        var target = "_system";

        try {
            const browser = this.iab.create("http://appsaz.ir", target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=بستن فرم');
        } catch (error) {
            console.log("inApp browser error");
        }
    }

    register() {
        this.nav.push(ActivationCodePage, {"action": "register"});
    }

    onsaleList() {
        this.nav.push(OnSaleListPage);
    }

    topsellersList() {
        this.nav.push(TopsellersPage);
    }


    gotoMessages() {
        this.nav.push(NotifListPage);
    }

    openBlog() {
        this.nav.push(BlogsPage);
    }

    openHome() {
        /*this.nav.push(Home);*/
        this.nav.setRoot(Home);
    }

    openMyItems() {
        if (this.values.isLoggedIn) {
            this.nav.push(MySpecialItemsPage);
        } else {
            this.showLoginToast();
        }
    }

    contactUs() {
        var target = "_self";

        try {
            const browser = this.iab.create(this.appsazSettings.contactUsUrl, target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=بستن فرم');
        } catch (error) {
            console.log("inApp browser error");
        }
    }

    policy() {
        var target = "_self";

        try {
            const browser = this.iab.create(this.appsazSettings.policyUrl, target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=بستن فرم');
        } catch (error) {
            console.log("inApp browser error");
        }
    }

    shareLink() {

        var msg = this.appsazSettings.shareLinkText;
        var options = {
            message: msg,
            /* chooserTitle: 'ارسال لینک از طریق'*/
            chooserTitle: 'Send link via'
        };

        this.socialSharing.shareWithOptions(options).then(() => {

        }).catch(() => {

        });

    }


}
