import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {NativeStorage} from '@ionic-native/native-storage';
import {CartService} from '../providers/service/cart-service';
import {WishlistService} from '../providers/service/wishlist-service';
import {CategoryService} from '../providers/service/category-service';
import {CheckoutService} from '../providers/service/checkout-service';
import {Config} from '../providers/service/config';
import {Functions} from '../providers/service/functions';
import {ProductService} from '../providers/service/product-service';
import {Service} from '../providers/service/service';
import {Values} from '../providers/service/values';
import {SocialSharing} from '@ionic-native/social-sharing';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP} from '@ionic-native/http/ngx';
import {MyApp} from './app.component';
import {Home} from '../pages/home/home';
import {AccountLogin} from '../pages/account/login/login';
import {OrderDetails} from '../pages/account/order-details/order-details';
import {Orders} from '../pages/account/orders/orders';
import {AccountRegister} from '../pages/account/register/register';
import {WishlistPage} from '../pages/account/wishlist/wishlist';
import {CartPage} from '../pages/cart/cart';
import {BillingAddressForm} from '../pages/checkout/billing-address-form/billing-address-form';
import {ProductPage} from '../pages/product/product';
import {ProductsPage} from '../pages/products/products';
import {KeysPipe} from '../providers/pipe/pipe';
import {NumberSeparatorPipe} from "../pipes/number-separator/number-separator";
import {ReviewsPage} from "../pages/reviews/reviews";
import {AboutUsPage} from "../pages/about-us/about-us";
import {EditInfoPage} from "../pages/account/edit-info/edit-info";
import {StarRatingModule} from 'ionic3-star-rating';
import {JalaliPipe} from "../pipes/jalali/jalali";
import {Deeplinks} from "@ionic-native/deeplinks";
import {ActivationCodePage} from "../pages/account/activation-code/activation-code";
import {SplashPage} from "../pages/splash/splash";
import {OnSaleListPage} from "../pages/on-sale-list/on-sale-list";
import {TopsellersPage} from "../pages/topsellers/topsellers";
import {FilterModalPage} from "../pages/filter-modal/filter-modal";
import {RoundPipe} from "../pipes/round/round";
import {OfflinePage} from "../pages/offline/offline";
import {Network} from "@ionic-native/network";
import {Device} from "@ionic-native/device";
import {AppsazSettings} from "../appsazSettings";
import {notificationService} from "../providers/service/notification-service";
import {NotifListPage} from "../pages/notif-list/notif-list";
import {FCM} from "@ionic-native/fcm";
import {AttributeKeyPipe} from "../pipes/attribute-key/attribute-key";
import {SearchModalPage} from "../pages/search-modal/search-modal";
import {GlobalSearchService} from "../providers/service/global-serach-service";
import {BlogsPage} from "../pages/blogs/blogs";
import {BlogDetailPage} from "../pages/blog-detail/blog-detail";
import {CouponModalPage} from "../pages/coupon-modal/coupon-modal";
import {FactorModalPage} from "../pages/factor-modal/factor-modal";
import { ComponentsModule } from '../components/components.module'
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { Market } from '@ionic-native/market';
import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import {CacheImgModule, ImgCacheService} from "../global/img-cache";
import { File } from '@ionic-native/file';
import {FeaturedListPage} from "../pages/featured-list/featured-list";
import {OnsaleService} from "../providers/service/onsale-service";
import {FeaturedService} from "../providers/service/featured-service";
import {AccountsService} from "../providers/service/accounts-service";
import {TopsellersService} from "../providers/service/topsellers-service";

import {GooglePlus} from "@ionic-native/google-plus";
import {ReorderModalPage} from "../pages/account/reorder-modal/reorder-modal";
import {MySpecialItemsPage} from "../pages/account/my-special-items/my-special-items";
import {CheckTimeAvailablePipe} from "../pipes/check-time-available/check-time-available";
import {LocalCartPage} from "../pages/local-cart/local-cart";
import {LocalCartServiceProvider} from "../providers/service/local-cart-service";

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,
        Home,
        AccountLogin,
        OrderDetails,
        Orders,
        AccountRegister,
        WishlistPage,
        CartPage,
        BillingAddressForm,
        ProductPage,
        ProductsPage,
        KeysPipe,
        NumberSeparatorPipe,
        RoundPipe,
        JalaliPipe,
        CheckTimeAvailablePipe,
        ReviewsPage,
        AboutUsPage,
        EditInfoPage,
        ActivationCodePage,
        SplashPage,
        OnSaleListPage,
        TopsellersPage,
        FeaturedListPage,
        FilterModalPage,
        CouponModalPage,
        SearchModalPage,
        FactorModalPage,
        OfflinePage,
        NotifListPage,
        AttributeKeyPipe,
        BlogsPage,
        BlogDetailPage,
        ReorderModalPage,
        MySpecialItemsPage,
        LocalCartPage
    ],
    imports: [
        CacheImgModule,
        CacheImgModule.forRoot(),
        ionicGalleryModal.GalleryModalModule,
        BrowserAnimationsModule,
        ComponentsModule,
        BrowserModule,
        HttpClientModule,
        HttpModule,
        IonicModule.forRoot(MyApp, {
            backButtonIcon: 'ios-arrow-forward',
            backButtonText: '',
            iconMode: 'ios',
            modalEnter: 'modal-slide-in',
            modalLeave: 'modal-slide-out',
            tabsPlacement: 'bottom',
            pageTransition: 'ios-transition',
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        StarRatingModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        Home,
        AccountLogin,
        OrderDetails,
        Orders,
        AccountRegister,
        WishlistPage,
        CartPage,
        BillingAddressForm,
        ProductPage,
        ProductsPage,
        ReviewsPage,
        AboutUsPage,
        EditInfoPage,
        ActivationCodePage,
        SplashPage,
        OnSaleListPage,
        TopsellersPage,
        FeaturedListPage,
        FilterModalPage,
        SearchModalPage,
        CouponModalPage,
        FactorModalPage,
        OfflinePage,
        NotifListPage,
        BlogsPage,
        BlogDetailPage,
        ReorderModalPage,
        MySpecialItemsPage,
        LocalCartPage
    ],
    providers: [
        File,

        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: ionicGalleryModal.GalleryModalHammerConfig,
        },
        StatusBar,
        SplashScreen,
        InAppBrowser,
        NativeStorage,
        SocialSharing,
        HTTP,
        CartService,
        WishlistService,
        CategoryService,
        CheckoutService,
        Config,
        Functions,
        ProductService,
        Service,
        Values,
        Deeplinks,
        Network,
        AppsazSettings,
        notificationService,
        Device,
        FCM,
        GooglePlus,
        GlobalSearchService,
        AccountsService,
        Market,
        ImgCacheService,
        CacheImgModule,
        OnsaleService,
        TopsellersService,
        CategoryService,
        FeaturedService,
        LocalCartServiceProvider,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
