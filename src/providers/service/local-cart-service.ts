import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NavController, ToastController} from 'ionic-angular';
import {Observable} from "rxjs";
import {Config} from './config';
import {catchError, tap} from "rxjs/operators";
import {Values} from "./values";
import {Plugins} from "@capacitor/core";
import {Functions} from "./functions";
import {Service} from "./service";
import {exit} from "ionicons/icons";

const {Storage} = Plugins;
const {Devices} = Plugins;

export interface CartItem {
    inventory?: number;
    managed_stock?: boolean;
    sold_individually?: boolean;
    product_name?: string;
    price_html?: any;
    thumbnail?: string;
    product_id: number;
    quantity: number;
    key?: string;
    variation_id?: number;
    line_total?: number;
}

export interface ShippingMethod {
    id: string;
    method_id: string;
    instance_id: string;
    label: string;
    cost: number;
}

export interface Cart {
    is_empty: boolean;
    total_html: string;
    count: number;
    subtotal: number;
    content_total: number;
    weight: string;
    shipping_methods: ShippingMethod[];
    coupons: any[];
    discount_total: number;
    needs_payment: boolean;
    needs_shipping: boolean;
    needs_shipping_address: boolean;
    show_shipping: boolean;
    items: CartItem[];
    currency_symbol: string;
}

@Injectable()
export class LocalCartServiceProvider {
    localCart: any[] = [];
    private cart: Cart;

    // cartWatcher: Subject<Cart> = new Subject<Cart>();

    constructor(public http: HttpClient,
                private config: Config,
                private toastCtl: ToastController,
                private service: Service,
                public values: Values,
                public functions: Functions) {
        // this.localCart.items=[];
        Storage.get({key: 'cart'}).then(cartData => {
            console.log("capacitor cartData constructor");
            var data = JSON.parse(cartData.value);
            if (data != null) {
                console.log('cartttt');
                console.log(data);
                this.localCart = data;

            }
        }, error => {
            console.error('cart error');
            console.error(error);
        });
    }

    async calculate(coupon?: any) {
        let cart: Observable<Cart>;
        Storage.get({key: 'cart'}).then(cartData => {
            var data = JSON.parse(cartData.value);
            if (data != null) {
                this.localCart = data;
            }
        }, error => {
            console.error('cart error');
            console.error(error);
        });
        let customer_id = 0;

        if (this.service.loggedInCustomer) {
            customer_id = this.service.loggedInCustomer.customer.id;
        }

        cart = this.http.post<Cart>(this.config.url + '/wp-json/wc/v3/cart/calculate', {
            items: this.localCart,
            coupon: coupon,
            // customer_id: this.values.customerId
            customer_id: customer_id
        });

        return cart.pipe(
            tap(cart => {
                this.set_cart(cart);
            })
        ).toPromise();
    }

    async checkout(form) {
        console.log(form);
        return this.http.post(this.config.url + '/wp-json/wc/v3/orders', {
            ...form,
        }).toPromise()
            .catch(err => {
                console.log(err);
                if (err.error && err.error.code == 'woocommerce_rest_invalid_coupon') {
                    this.functions.showAlertError("", 'کوپن استفاده شده متعبر نمی باشد.');
                } else {
                    this.functions.showAlertError("", err.error.message);
                }
            })
    }

    emptyCart() {
        this.localCart = [];
        this.update()
    }


    async calculate2(data?: any) {
        let cart: Observable<Cart>;
        cart = this.http.post<Cart>(this.config.url + '/wp-json/wc/v3/cart/calculate', {
            items: data
        });
        console.log(cart);

        return cart.pipe(
            tap(cart => {
                this.localCart = data;
                this.set_cart(cart);
                console.log('ffff', cart);
            })
        ).toPromise();
    }


    get_local_item(productId: number, variationId?: number) {
        // if (variationId) {
        //     return this.localCart.find(item => item.product_id === variationId);
        // } else {
        // }
        return this.localCart.find(item => item.product_id === productId);
    }

    private get_item(key: string) {
        return this.cart.items.find(item => item.key === key);
    }

    async add(productId: number, quantity: number = 1, showAlert: boolean = true, variationId?: number) {
        console.log(productId);
        Storage.get({key: 'cart'}).then(cartData => {
            console.log("capacitor cartData")
            var data = JSON.parse(cartData.value);
            if (data != null) {
                console.log('jjjjj', data);
                let index = data.map(o => o.product_id).indexOf(productId);
                console.log('indexxxx:', index);
                if (index != -1) {
                    data[index].quantity += quantity;
                } else {
                    data.push({product_id: productId, quantity: quantity})
                }

                this.localCart = data;
                Storage.set({key: 'cart', value: JSON.stringify(data)});

                if (showAlert) {
                    let toast = this.toastCtl.create({
                        position: 'bottom',
                        message: 'Added to cart.',
                        cssClass: 'rtl',
                        duration: 2500,
                    });
                    toast.present();
                }
                this.values.count++;

                // }

            }
        }, error => {
            console.error('cart error');
            console.error(error);
        });
        await this.update();
    }

    async remove(productId: number, variationId?: number) {
        // if (variationId) {
        //     this.localCart = this.localCart.filter(item => item.product_id !== variationId);
        // } else {
        // }
        this.localCart = this.localCart.filter(item => item.product_id !== productId);
        console.log('remove item');
        console.log(this.localCart);
        this.update();

    }


    async apply_coupon(coupon: string) {
        return this.http.post(this.config.url + '/wp-json/wc/v3/cart/coupon', {
            coupon
        }, this.config.options).toPromise();
    }

    async remove_coupon(coupon: string) {
        return this.http.delete(this.config.url + '/wp-json/wc/v3/cart/coupon', {
            params: {
                coupon
            }
        }).toPromise();
    }

    private set_cart(cart: Cart) {
        console.log('gggg', cart);
        this.cart = cart;
        // this.cartWatcher.next(cart);
    }

    private async update(partial: boolean = false) {
        console.log(this.localCart);
        await Storage.set({key: 'cart', value: JSON.stringify(this.localCart)});
        this.values.count = this.localCart.length;
        await this.calculate();
    }

    public getProductQuantityInCart(product_id) {
        console.log('asdsadasasd', this.localCart);
        let product = null;

        this.localCart.forEach(prd => {
            if (prd.product_id == product_id) {
                product = prd;
            }
        });

        if (product) {
            return product['quantity'];
        } else {
            return null;
        }
    }

    async addLocal(productId: number, quantity: number = 1, showAlert: boolean = true, variationId?: number) {
        Storage.get({key: 'cart'}).then(cartData => {
            var data = JSON.parse(cartData.value);
            if (data != null) {
                let index = data.map(o => o.product_id).indexOf(productId);
                if (index != -1) {
                    data[index].quantity = quantity;
                } else {
                    data.push({product_id: productId, quantity: quantity})
                }

                this.localCart = data;
                Storage.set({key: 'cart', value: JSON.stringify(data)});

                if (showAlert) {
                    let toast = this.toastCtl.create({
                        position: 'bottom',
                        message: 'Added to cart.',
                        duration: 2500,
                    });
                    toast.present();
                }
                this.values.count++;
            }
        }, error => {
            console.error('cart error');
            console.error(error);
        });

        await Storage.set({key: 'cart', value: JSON.stringify(this.localCart)});
        this.values.count = this.localCart.length;
    }

}

