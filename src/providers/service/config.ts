import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Headers } from '@angular/http';
declare var oauthSignature: any;
var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

@Injectable()
export class Config {

    // url: any = 'https://mediator.appsaz.pro';
    // url: any = 'https://mediatore.ir';
    // consumerKey: any = 'ck_8b53b82017606e44c84498abb9298c41119fb415';
    // consumerSecret: any = 'cs_cc69255b71f4eea0a1275883abb0c181b4364015';


    // url: any = 'http://app.fereshtegan-leather.com';
    // consumerKey: any = 'ck_bb2ab4c93f5262faef01ef9fe1f565df9204453d';
    // consumerSecret: any = 'cs_8dac4cadf3a943a63876f7525642ea5dae0c89e7';


    // url: any = 'https://andromedadesign.ir';
    // consumerKey: any = 'ck_bbf4c862d6b931a057004accdc2410d5dd754788';
    // consumerSecret: any = 'cs_cc79fce6d728e9ff7856902125f16fefef7434c8';

    //url: any = 'https://appsazshop.ir';
    url: any = 'https://danmad.xyz';
    consumerKey: any = 'ck_3daf774d7f10499d6ade4f7fc1a08748efcede0a';
    consumerSecret: any = 'cs_a3997c84e5cfe7587e1bc2ab08e74ee4071e1b0d';

    // url: any = 'http://salembekhar.com';
    // consumerKey: any = 'ck_72dacdeff5f1e00c0fe2d3c931ac22e5cac1482e';
    // consumerSecret: any = 'cs_19999def9a34b251d3c918ceffe63c1fddc70489';


    // url: any ='http://hikvision.vision';// 'http://np.appsazapps.ir';//'http://localhost/newNels';//'http://localhost/npstore';// // Repleace http://example.com your site url
    // consumerKey: any ='ck_c87749d3c87cb5cb8f44a6f7cd1479c180010086';//'ck_5e3b51b0d9eb14f230453681800fbd586a134dd4';//'ck_5606b95da62db3e2f7164b166595be45259ec967';//// 'ck_ded535751bf5dd49b7c5ad9e5681aa69de1d0594';// // Repleace CONSUMER_KEY_PLACEHOLDER your site Consumer Key
    // consumerSecret: any ='cs_45aa2313d49fb175643a34397ccd2a3b705cda1c';//'cs_c504eaf876cf50ddffd2bb45fbe2556852736792'; //'cs_4e8a91f879d90b586d745445f539569b9cd2d7a5';// 'cs_69e49fc2593d390d5bcb9ab2592f0018b43b5df3';//// Repleace CONSUMER_SECRET_PLACEHOLDER your site Consumer Secret


  /*  url: any = 'https://danmad.dk';
    consumerKey: any = 'ck_3daf774d7f10499d6ade4f7fc1a08748efcede0a';
    consumerSecret: any = 'cs_a3997c84e5cfe7587e1bc2ab08e74ee4071e1b0d';*/

    oauth: any;
    signedUrl: any;
    randomString: any;
    oauth_nonce: any;
    oauth_signature_method: any;
    encodedSignature: any;
    searchParams: any;
    customer_id: any;
    params: any;
    options: any = {};

    constructor() {
        this.options.withCredentials = true;
        this.options.headers = headers;
        this.oauth = oauthSignature;
        this.oauth_signature_method = 'HMAC-SHA1';
        this.searchParams = new URLSearchParams();
        this.params = {};
        this.params.oauth_consumer_key = this.consumerKey;
        this.params.oauth_signature_method = 'HMAC-SHA1';
        this.params.oauth_version = '1.0';
    }
    setOauthNonce(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    }
    setUrl(method, endpoint, filter) {
        var key;
        var unordered = {};
        var ordered = {};
        if (this.url.indexOf('https') >= 0) {
            unordered = {};
            if (filter) {
                for (key in filter) {
                    unordered[key] = filter[key];
                }
            }
            unordered['consumer_key'] = this.consumerKey;
            unordered['consumer_secret'] = this.consumerSecret;
            Object.keys(unordered).sort().forEach(function(key) {
                ordered[key] = unordered[key];
            });
            this.searchParams = new URLSearchParams();
            for (key in ordered) {
                this.searchParams.set(key, ordered[key]);
            }
            return this.url + endpoint + this.searchParams.toString();
        }
        else {
            var url = this.url + endpoint;
            this.params['oauth_consumer_key'] = this.consumerKey;
            this.params['oauth_nonce'] = this.setOauthNonce(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            this.params['oauth_timestamp'] = new Date().getTime() / 1000;
            for (key in this.params) {
                unordered[key] = this.params[key];
            }
            if (filter) {
                for (key in filter) {
                    unordered[key] = filter[key];
                }
            }
            Object.keys(unordered).sort().forEach(function(key) {
                ordered[key] = unordered[key];
            });
            this.searchParams = new URLSearchParams();
            for (key in ordered) {
                this.searchParams.set(key, ordered[key]);
            }
            this.encodedSignature = this.oauth.generate(method, url, this.searchParams.toString(), this.consumerSecret);
            return this.url + endpoint + this.searchParams.toString() + '&oauth_signature=' + this.encodedSignature;
        }
    }
}
