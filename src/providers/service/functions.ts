import { Injectable } from '@angular/core';
import {AlertController, LoadingController, ToastController} from 'ionic-angular';
import { Values } from './values';

@Injectable()
export class Functions {
    loader: any;
    constructor(private alert: AlertController,
                private loadingController: LoadingController,
                private values: Values,
                private toastCtrl: ToastController) {
    }
    showAlertWarning(title, text) {
        let alert = this.alert.create({
            title: title,
            subTitle: text,
            cssClass: 'msg-class-warning',
            /*buttons: ['تایید']*/
            buttons: ['OK']
        });
        alert.present();
    }

    showAlertError(title, text) {
        let alert = this.alert.create({
            title: title,
            subTitle: text,
            cssClass: 'msg-class-error',
            /*buttons: ['تایید']*/
            buttons: ['OK']
        });
        alert.present();
    }

    showAlertSuccess(title, text) {
        let alert = this.alert.create({
            title: title,
            subTitle: text,
            cssClass: 'msg-class-success',
            /*buttons: ['تایید']*/
            buttons: ['OK']
        });
        alert.present();
    }

    showAlertMessage(title, text) {
        let alert = this.alert.create({
            title: title,
            subTitle: text,
            cssClass: 'msg-class-success',
            /*buttons: ['تایید']*/
            buttons: ['OK']
        });
        alert.present();
    }

    showToast(text){
        let toast = this.toastCtrl.create({
            message: text,
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
        });
        toast.onDidDismiss(() => {

        });
        toast.present();
    }

}
