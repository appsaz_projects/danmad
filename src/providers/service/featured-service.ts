import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Http} from '@angular/http';
import {Functions} from "./functions";
import {Plugins} from "@capacitor/core";

@Injectable()
export class FeaturedService {

    featured_loading : boolean = true;
    has_more_featured_items: boolean = true;
    featured_filter: any = {};
    featured: any;

    constructor(public http: Http,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient) {

        this.featured_filter.page = 1;
    }


    loadMore_featured() {
        this.featured_filter.page += 1;
        this.featured_filter.featured = true;
        this.featured_filter.status = 'publish';

        return new Promise(resolve => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v2/products/?', this.featured_filter)).subscribe(data => {

                this.handleMore_featured(data);
                resolve(true);
            });
        });
    }



    getFeatured(): Promise<any> {
        this.featured_loading = true;
        console.log("getFeatured called");
        return new Promise((resolve, reject) => {

            this.featured_filter.featured = true;
            this.featured_filter.status = 'publish';

            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/products?', this.featured_filter)).subscribe(data => {
                this.featured = data;
                this.featured_loading = false;
                console.log("getFeatured success");

                resolve(true);

            }, error => {
                console.log(error);
                reject(false);
            });
        });
    }

    handleMore_featured(results) {
        if (results != undefined) {

            for (var i = 0; i < results.length; i++) {
                this.featured.push(results[i]);
            }
        }
        if (results.length == 0) {
            this.has_more_featured_items = false;
        }
    }




}
