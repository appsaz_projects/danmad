import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the CheckTimeAvailablePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'checkTimeAvailable',
})
export class CheckTimeAvailablePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
   // return value.toLowerCase();
    console.log("value:", value)
    var startTime= value.substring(0,value.indexOf('-'));
    var endTime= value.substring(value.indexOf('-') + 1);
    console.log("startTime:", startTime)
    console.log("endTime:", endTime)
    var d = new Date();
    var n = d.getHours();
    
    if(parseInt(endTime)< n || parseInt(startTime)< n){
      return false;
    }
    else{
      return true;
    }

    
  }
}
