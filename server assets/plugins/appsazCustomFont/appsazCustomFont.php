<?php
/*
Plugin Name: appsazCustomFont
Plugin URI:
Description: Simple plugin to replace font(s) in WordPress Admin Dashboard with b koodak. inspired by TehnoBlog.org
Author: Asghar Golkar
Author URI:
Version: 0.0.1
*/


if ( !defined('ABSPATH') ) {
    exit;
}

// WordPress Custom Font @ Admin
function ag_admin_custom_font() {
   $uri= plugin_dir_url(__FILE__);
    echo '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">' . PHP_EOL;
    echo '<style> 
    @font-face {
		font-family: "bkoodak";
		src: url("'.$uri.'/font.ttf");
		}
   
    body, #wpadminbar *:not([class="ab-icon"]), .wp-core-ui, .media-menu, .media-frame *, .media-modal *{font-family:"bkoodak",sans-serif !important;}</style>' . PHP_EOL;
}
add_action( 'admin_head', 'ag_admin_custom_font' );
